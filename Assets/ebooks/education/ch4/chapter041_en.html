<p>
    <strong>Introduction</strong>
</p>
<p>
    Each of us has different ways of handling stress, yet some methods may not
    be effective in helping us to truly relax. For example, people may find
    short-term excitement through playing computer games, yet the numbing
    sensation goes as quickly as it comes.
</p>
<p>
    It is important to have some strategies to combat stress. We recommend the
    following strategy called Operation “SMILE” for your reference.
    <div align="center">
        <img src="images/soical_support_en.png" alt="soical_support" style="max-width: 100%">
        <img src="images/mind_work_en.png" alt="mind_work" style="max-width: 100%">
        <img src="images/lifestyle_en.png" alt="lifestyle" style="max-width: 100%">
        <img src="images/relaxation_exercise_en.png" alt="relaxation_exercise" style="max-width: 100%">
    </div>
</p>
<p>
    <strong>S</strong>ocial Support
</p>
<p>
    Police officers often give others an impression of toughness and
    independence. Some officers choose to shoulder all the responsibilities
    because they think others may not be able to help or do not want to burden
    others. In fact, strength comes from a well-supported social network. If we
    are willing to be listened to and understood, and to accept others’ care
    and recognition, our stress-induced negative emotions can be relieved. Many
    research studies have shown that social support (i.e. receiving help or
    care from others) is associated with better health:
</p>
<ul>
    <li>
        Social support can enhance our resilience and help mediate the negative
        impact of work stress
    </li>
    <li>
        When needed, your family, friends and colleagues can provide you with
        practical assistance or different points of view
    </li>
    <li>
        Engaging in sports or leisure activities with friends can foster
        positive emotions
    </li>
</ul>
<p>
    A good support system needs cultivation, especially the family network. The
    nature of police work may at times affect a family system:
</p>
<ul>
    <li>
        Shift work may affect the time spent with family
    </li>
    <li>
        The police culture and practices, such as emphasis on authority, a
        directive and suspicious attitude, may create conflicts with family
        members and hamper trust
    </li>
    <li>
        It is common for officers to adopt similar disciplines at home (<em>policing the family</em>). We may easily forget that each family
        member has their unique personality and needs. Bringing home a set of
        rules practised in the police culture may damage our family
        relationships
    </li>
    <li>
        Family members who are not members of the Force may not be familiar
        with the police work, resulting in misunderstandings
    </li>
    <li>
        Officers believe that they should not burden the family with their work
        stress. They often keep everything to themselves which may affect
        communication within the family and easily create misunderstandings
    </li>
    <li>
        Officers may unintentionally transfer their negative emotions onto
        their family members
    </li>
</ul>
<p>
    Relationships are like savings account:
</p>
<img src="images/account_en.png" alt="account" style="max-width: 100%">
<p>
    Officers need to keep the family relationship account in balance, so that
    family can become the most stable support network for combating stress.
</p>
<p>
    <strong>MI</strong>nd Work
</p>
<p>
    “People are not disturbed by things, but by the view we take of them” —
    Epictetus, Philosopher
</p>
<p>
    Our emotions and behaviors are affected by the way we think. At times, our
    thoughts may be unreasonable or distorted. If we do not want to be
    controlled by our thoughts, we need to learn to develop an objective way of
    thinking.
</p>
<p>
    <em>
        For instance, a police constable failed in a sergeant promotion
        interview. He thought, “I am so useless and incompetent. There is no
        hope for me in the future to be promoted to a sergeant.”
    </em>
</p>
<ol>
    <li>
        <strong>
            Be aware of the appearance of irrational thoughts. Remind ourselves
            that thoughts are just thoughts, but not necessarily the reality
        </strong>
        <p>
            <em>
                Police Constable: “When I feel low my thoughts would naturally be more
                pessimistic. They may not reflect the reality.”
            </em>
        </p>
    </li>
    <li>
        <strong>
            Appraise the validity of our thoughts objectively. Try to explore
            other alternatives from multiple perspectives.
        </strong>
        <ol>
            <li>
                Find evidence that supports or refutes these thoughts
                <p>
                    <em>
                        Police Constable: “Apart from this single failure, are there other
                        evidences showing that I am useless and incompetent? Are all colleagues
                        who fail to be promoted useless and incompetent?”
                    </em>
                </p>
            </li>
            <li>
                Look for other possibilities
                <p>
                    <em>
                        Police Constable: “What are the other reasons for my promotion failure
                        this time? Was I too nervous? Or lacking interview skills?”
                    </em>
                </p>
            </li>
        </ol>
    </li>
    <li>
        <strong>
            Replace our thoughts with more positive and plausible alternatives
        </strong>
        <ol>
            <li>
                Even if our thoughts are true, are these thoughts
                constructive?
                <p>
                    <em>
                        Police Constable: “It does me no good if I continue with these negative
                        thoughts. They will only bring me down further.”
                    </em>
                </p>
            </li>
            <li>
            <p>
                Even if some of our thoughts are true, so what? Their consequences may
                not be as serious as we think
            </p>
                <p>
                    <em>
                        Police Constable: “Even if I am really unsuitable for being a sergeant,
                        it is not the end of the world. I can still continue with my job. I can
                        also spend my leisure time with my family and establish other
                        accomplishments outside work.”
                    </em>
                </p>
            </li>
        </ol>
    </li>
</ol>
<p>
    Through reconstructing our thoughts, you will begin to find changes in your
    emotions. Try starting your mind work through awareness, appraisal and
    replacement of negative thoughts to regulate stress and emotions!
</p>
<br>
<p>
    <strong>L</strong>ifestyle
</p>
<br>
<p>
    Combating stress is not a day’s work. It is important to develop a healthy
    lifestyle to strengthen our resilience to stress.
</p>
<ol type="A">
    <li>
        <strong>Quality Sleep</strong>
        <p>
            Our quality of sleep is most easily affected when we are under stress.
            Occasional insomnias are actually very common and they may not impact our
            daily living a lot. Therefore, we do not have to be too distressed when it
            is time for bed. We can just keep in mind that lying in bed itself is
            already helping us to rewind and relax.
        </p>
        <p>
            Here are some tips to help you sleep better:
        </p>
        <ol>
            <li>
                Reduce drinking alcohol or caffeinated drinks such as coffee or tea
            </li>
            <li>
                Avoid doing strenuous exercises or playing computer games before sleep
            </li>
            <li>
                Avoid sleeping in daytime unless you have night work shifts
            </li>
            <li>
                Maintain a regular resting schedule. Even if you fall asleep later than
                usual, try to wake up at a fixed time
            </li>
            <li>
                Make sure your bed is a place for rest and relaxation. Do not work on
                your bed
            </li>
            <li>
                Keep your bedroom environment comfortable (maintain a pleasant room
                temperature and choose comfortable bedding furniture)
            </li>
            <li>
                Perform relaxation or breathing exercises before sleep to slow down the
                busy thoughts in head
            </li>
        </ol>
        <br>
    </li>
    <li>
        <strong>Quality Exercises</strong>
        <br>
        <p>
            Regular and appropriate amount of exercises is key to stress reduction.
            Studies found that performing    <strong>aerobic exercises for at least 20 minutes</strong> will help the
            brain to release endorphin, which helps produce pleasurable feelings and
            regulate our emotions.
        </p>
        <p>
            Exercise can help relax the tension in our muscles. It can increase blood
            flow to our brain, helping us to stay clear-headed and sleep better, and
            keep unpleasant thoughts away. Exercising with friends or families can also
            create opportunities for social bonding, enhancing our motivation and
            satisfaction.
        </p>
        <p>
            Exercising requires perseverance. Light exercises such as jogging for 30-60
            minutes are enough to help you stay healthy. If you do not exercise often
            but suddenly perform a large amount of strenuous exercises at one time, not
            only will you be unable to reduce stress, but you may also potentially do
            harm to your body.
        </p>
    </li>
    <li>
        <strong>Quality Diet</strong>
        <p>
            A balanced diet is essential for mental health. Bad eating habits would
            affect our ability to think, analyse, concentrate and memorise, especially
            the following ‘toxic’ food:
        </p>
        <ol>
            <li>
                <strong>Alcohol: </strong>
                excessive alcohol intake affects our sleep and dehydrates our body. It
                impairs bodily functions and slows down reflexes, affecting our ability
                to deal with stress.
            </li>
            <li>
                <strong>Smoking: </strong>
                nicotine will cause an increase in heart rate, and damages to our lungs
                and airways.
            </li>
        </ol>
    </li>
    <li>
        <strong>Financial Management</strong>
        <p>
            Officers enjoy a more stable and generous income compared to other
            professions with similar academic qualifications. Therefore, some may
            overestimate their spending power and over-spend on high-end entertainment.
            Also, some may think that gambling can reduce stress, or hope to win big
            with small stakes, resulting in financial debt.
        </p>
        <p>
            Not only can good financial management reduce financial burden, it can also
            allow us to enjoy the fruits of our hard work. Here are some tips for
            financial management:
        </p>
        <ol>
            <li>
                Know your financial situation, such as income, savings, loan, interest
                etc.
            </li>
            <li>
                Carefully allocate your money for savings and expenditures. Besides
                recurrent living expenses, reserve a portion of cash flow for emergency
                use
            </li>
            <li>
                Choose a saving plan that best suits your needs and develop a saving
                habit
            </li>
            <li>
                Invest carefully. Do not be greedy because of petty advantages
            </li>
        </ol>
    </li>
    <li>
        <strong>Time Management</strong>
        <p>
            Working non-stop and skipping meals will not boost our work efficiency, but
            instead it will impair our work performance. It is important to learn to
            prioritize such that we do not waste our time and energy on trivial things.
            Remind yourself to balance your time between work, family, social events,
            and developing your interests. Reserving some “me-time” for yourself, and
            allowing yourself to enjoy peace and things you like are also good ways to
            reduce stress.
        </p>
        <p>
            To better manage time, we need to increase our work efficiency and reduce
            our waste of time. You can try:
        </p>
        <ol>
            <li>
                Keep your office and home organized to avoid wasting time on searching
                for things.
            </li>
            <li>
                Make a to-do list and prioritize the tasks in terms of urgency. This
                will ensure you have enough time to complete the important tasks.
            </li>
            <li>
                Establish a good filing system (e.g. using files or folders) to reduce
                the time spent on searching for documents.
            </li>
            <li>
                Complete your tasks at one go and avoid unnecessary re-readings.
            </li>
            <li>
                Make time for breaks in your schedule so that you can be recharged, and
                your efficiency will be boosted.
            </li>
            <li>
                Learn to say “no” when appropriate.
            </li>
        </ol>
    </li>
</ol>
<br>
<p>
    <strong>R</strong>elaxation <strong>E</strong>xercise
</p>
<p>
    Police work is busy and nerve-racking. We seem to be constantly in a battle
    mode. If this battle mode is turned on for a prolonged period of time, the
    stress hormones in our body will remain at a constant high. This will
    weaken our immune system and cognitive functions.
</p>
<p>
    Systematic relaxation exercises can help us to transit from a battle mode
    to a state of relaxation. Once we enter the state of relaxation, our
    metabolic rates will decrease and our stress and fatigue can be relieved.
    This can also help us to increase our concentration level and work
    efficiency.
</p>
<br>
<p>
    <strong>Relaxation Exercises</strong>
</p>
<p>
    Find a quiet place and sit on a chair with backrest and armrests. Place
    your feet on the ground and close your eyes. Practise the following two
    relaxation techniques once or twice daily.
</p>
<ol>
    <li>
        <strong>Diaphragmatic Breathing</strong>
        <ul>
            <li>
                Let yourself breathe naturally. Focus on the speed and sound of your
                breathing
            </li>
            <li>
                Slowly breathe in through your nose, until your stomach slightly
                expands. Stop breathing and count to three silently
            </li>
            <li>
                Slowly breathe out
            </li>
            <li>
                Concentrate on your exhalation. In your mind, keep thinking that you
                are “calm” and “relaxed”
            </li>
            <li>
                Repeat the exercise 10 to 12 times
            </li>
            <li>
                You may refer to these websites for references:
            </li>
        </ul>
        <iframe width="400" height="225" src="https://www.youtube.com/embed/DMEpbGW5NSU" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
        <a href="https://m.youtube.com/watch?v=DMEpbGW5NSU">
            https://m.youtube.com/watch?v=DMEpbGW5NSU
        </a>
        <br><br>
        <iframe width="400" height="225" src="https://www.youtube.com/embed/IXy053twOT0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
        <a href="https://www.youtube.com/watch?v=IXy053twOT0">
            https://www.youtube.com/watch?v=IXy053twOT0
        </a>
    </li>
    <li>
        <strong>Progressive Muscle Relaxation </strong>
        <ul>
            <li>
                Beginning with your hands—clench your fists for a few seconds and then
                relax
            </li>
            <li>
                Then repeat the procedures of tensing and relaxing the muscles of
                different body parts until your whole body is relaxed
            </li>
            <li>
                You may refer to this website for reference:
                <a href="https://www.youtube.com/watch?v=EFXGiRu4FR0">
                    https://www.youtube.com/watch?v=EFXGiRu4FR0
                </a>
            </li>
        </ul>
    </li>
</ol>
<iframe width="400" height="225" src="https://www.youtube.com/embed/EFXGiRu4FR0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
<p>
    It is best to seek assistance of professionals (e.g. clinical
    psychologists) in order to learn these techniques effectively. If you are
    interested in learning the above relaxation exercises, please feel free to
    contact the Psychological Services Group. (Tel: 2866 6206)
</p>