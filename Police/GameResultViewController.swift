//
//  GameResultViewController.swift
//  Police
//
//  Created by Aluminum on 8/18/17.
//  Copyright © 2017 Crossover. All rights reserved.
//

import UIKit
import Localize_Swift

class GameResultViewController: UIViewController {
    private let TAG: String = "GameResultViewController"
    
    @IBOutlet var summaryLabel: UILabel!
//    @IBOutlet var scoreLabel: UILabel!
    @IBOutlet var doneButton: UIButton!
    
    var score: Int!
    var isLevelUp: Bool!
    
    class func gameResultViewControllerForView(_ score: Int, _ upgrade: Bool) -> GameResultViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "GameResultViewController") as! GameResultViewController
        viewController.score = score
        viewController.isLevelUp = upgrade
        return viewController
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //scoreLabel.text = String(score)
        
        // Locale
        NotificationCenter.default.addObserver(self, selector: #selector(onLanguageChange), name: NSNotification.Name(LCLLanguageChangeNotification), object: nil)
        onLanguageChange()
    }
    
    @objc func onLanguageChange() {
        summaryLabel.text = "message_your_score".localized()
        doneButton.setTitle("action_done".localized(), for: .normal)
    }

    @IBAction func onDonePressed(_ sender: Any) {
        if let parentVC = parent as? GameViewController {
            parentVC.dismiss(animated: true, completion: nil)
        } else {
            NSLog("[\(TAG)] Unable to cast parent view controller to GameViewController")
        }
    }
}
