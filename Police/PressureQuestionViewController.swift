//
//  PressureQuestionViewController.swift
//  Police
//
//  Created by Crossover on 11/6/2018.
//  Copyright © 2018年 Crossover. All rights reserved.
//

import UIKit
import Localize_Swift

class PressureQuestionViewController: UIViewController {
    private let TAG: String = "PressureQuestionViewController"
    @IBOutlet var questionLabel: UILabel!
    @IBOutlet weak var numberLabel: UILabel!
    @IBOutlet weak var returnButton: UIButton!
    // Question Button list
    @IBOutlet weak var alwaysButton: UIButton!
    @IBOutlet weak var sometimeButton: UIButton!
    @IBOutlet weak var selfdomButton: UIButton!
    @IBOutlet weak var neverButton: UIButton!
    
    var question: TestSectionInfo!
    var answerTableVC: PressureQuestionTableViewController!
    
    var selectedAnswer = 0, finalScore: Int = 0, s1score:Int = 0, s2score:Int = 0, s3score:Int = 0
    var questionList: [String] = []
    var currentNum = 1;
    
    var helpButton: UIBarButtonItem?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        // Set up the Navgation Bar item
        helpButton = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_menu_help"), style: .plain, target: self, action: #selector(gettingHelp(_:)))
        navigationItem.rightBarButtonItem = helpButton
        
        returnButton.setTitle("action_return".localized(), for: .normal)
        
        // Add the questions into the array
        let testMgr =  PressureManager()
            for section in testMgr.getTestSections() {
                for question in section.mQuestions {
                    questionList.append(question)
                }
            }
        // Set the text to current question
        setupText()
        // Set a action into the button
        returnButton.isHidden = true
        
        alwaysButton.setTitle("pressure_always".localized(), for: UIControlState.normal)
        sometimeButton.setTitle("pressure_sometime".localized(), for: UIControlState.normal)
        selfdomButton.setTitle("pressure_selfdom".localized(), for: UIControlState.normal)
        neverButton.setTitle("pressure_never".localized(), for: UIControlState.normal)
        alwaysButton.addTarget(self, action: #selector(self.buttonClicked), for: .touchUpInside)
        sometimeButton.addTarget(self, action: #selector(self.buttonClicked), for: .touchUpInside)
        selfdomButton.addTarget(self, action: #selector(self.buttonClicked), for: .touchUpInside)
        neverButton.addTarget(self, action: #selector(self.buttonClicked), for: .touchUpInside)
    }
    
    @objc func gettingHelp(_ sender: UIBarButtonItem) {
        let alert = UIAlertController(title: nil, message: "message_pressure_test".localized(), preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "action_ok".localized(), style: .cancel))
        present(alert, animated: true, completion: nil)
    }
    
    @objc private func buttonClicked() {
        // Call when all button clicked
         if currentNum == questionList.count{
            // All questions have been answered
            // Execute DataBase
            /*
            let date = Date()
            var dbHelper = PoliceDBHelper()
            let statement = "INSERT INTO \(PoliceContract.PressureTestEntry.TABLE_NAME) (\(PoliceContract.PressureTestEntry.COLUMN_NAME_MARK), \(PoliceContract.PressureTestEntry.COLUMN_NAME_DATE)) VALUES ('\(finalScore)', '\(convertDateFormater(date))')"
            
            dbHelper?.execute(statement)
            dbHelper = nil
            print(finalScore)
            NSLog("The Final Score is " + "\(finalScore)")*/
            
            // Setting
            let defaults = UserDefaults.standard
            defaults.set(s1score, forKey: "presstest-score1")
            defaults.set(s2score, forKey: "presstest-score2")
            defaults.set(s3score, forKey: "presstest-score3")
            defaults.set(finalScore, forKey: "presstest-finalScore")

            
            questionDone()
         } else if currentNum < questionList.count{
            currentNum += 1
            setupText()
            returnButton.isHidden = false
        }
    }
    
    @IBAction func returnClicked(_ sender: Any) {
        if currentNum != 1 && currentNum < questionList.count{
            currentNum -= 1
            finalScore -= selectedAnswer
            setupText()
            returnButton.isHidden = true
        }
    }
    
    func setupText() {
        questionLabel.text = questionList[currentNum-1]
        numberLabel.text = String(format: "question_count_item".localized(), arguments: [currentNum])
    }
    
    @IBAction func add3Score(_ sender: UIButton) {
        selectedAnswer = 3
        finalScore += selectedAnswer
        s3score += selectedAnswer
    }
    
    @IBAction func add2Score(_ sender: UIButton) {
        selectedAnswer = 2
        finalScore += selectedAnswer
        s2score += 1
    }
    
    @IBAction func add1Score(_ sender: UIButton) {
        selectedAnswer = 1
        finalScore += selectedAnswer
        s1score += 1
    }
    
    @IBAction func add0Score(_ sender: UIButton) {
        selectedAnswer = 0
        finalScore += selectedAnswer
    }
    
    func questionDone(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "PressureChartViewController") as! PressureChartViewController
        viewController.finalScore = finalScore
        self.navigationController?.popViewController(animated: true)
        self.parent?.present(viewController, animated: true, completion: nil)
    }
    
    func convertDateFormater(_ date: Date) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "d MMM"
        return dateFormatter.string(from: date)
    }
}
