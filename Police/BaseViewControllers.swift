//
//  MainMenuViewController.swift
//  Police
//
//  Created by Aluminum on 7/27/17.
//  Copyright © 2017 Crossover. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {
    
    var revealButton: UIBarButtonItem?
    var settingsButton: UIBarButtonItem?
    
    override func viewDidLoad() {
        revealButton = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_option_menu"), style: .plain, target: self, action: #selector(revealNavBar(_:)))
        settingsButton = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_option_settings"), style: .plain, target: self, action: #selector(displaySettings(_:)))
        
        navigationItem.leftBarButtonItem = revealButton
        navigationItem.rightBarButtonItem = settingsButton
    }
    
    @objc func revealNavBar(_ sender: UIBarButtonItem) {
        if let drawer = self.navigationController?.parent as? KYDrawerController {
            drawer.setDrawerState(.opened, animated: true)
        }
    }
    
    @objc func displaySettings(_ sender: UIBarButtonItem) {
        if let viewController = self.storyboard?.instantiateViewController(withIdentifier: "SettingsNavController") {
            viewController.modalPresentationStyle = .popover
            viewController.popoverPresentationController?.barButtonItem = sender
            self.present(viewController, animated: true, completion: nil)
        }
    }
}

class BaseTableViewController: UITableViewController {
    
    var revealButton: UIBarButtonItem?
    var settingsButton: UIBarButtonItem?
    
    override func viewDidLoad() {
        revealButton = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_option_menu"), style: .plain, target: self, action: #selector(revealNavBar(_:)))
        settingsButton = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_option_settings"), style: .plain, target: self, action: #selector(displaySettings(_:)))
        
        navigationItem.leftBarButtonItem = revealButton
        navigationItem.rightBarButtonItem = settingsButton
    }
    
    @objc func revealNavBar(_ sender: UIBarButtonItem) {
        if let drawer = self.navigationController?.parent as? KYDrawerController {
            drawer.setDrawerState(.opened, animated: true)
        }
    }
    
    @objc func displaySettings(_ sender: UIBarButtonItem) {
        if let viewController = self.storyboard?.instantiateViewController(withIdentifier: "SettingsNavController") {
            viewController.modalPresentationStyle = .popover
            viewController.popoverPresentationController?.barButtonItem = sender
            self.present(viewController, animated: true, completion: nil)
        }
    }
}
