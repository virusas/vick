//
//  SettingsTableViewController.swift
//  Police
//
//  Created by Aluminum on 7/31/17.
//  Copyright © 2017 Crossover. All rights reserved.
//

import UIKit
import Localize_Swift

class SettingsTableViewController: UITableViewController {
    
    private let TAG: String = "SettingsTableViewController"
    
    static let sectionNames = [
        // Remember to mark these to Localizable.strings
        "action_user_profile",
        "pref_title_app_lang",
        "pref_title_text_size",
        "pref_about_title"
    ]
    static let rowNames = [
        // Remember to mark these to Localizable.strings
        [nil],
        ["zh-TW"],
        ["size_small",
         "size_medium",
         "size_large"],
        ["pref_title_installation_id",
         "pref_title_user_level"]
    ]
    
    static let langValues = [
        "en",
        "zh-Hant"
    ]
    static let sizeValues = [
        // iOS read this as zoom presentage instead of Android's size in pt
        20,
        100,
        300
    ]

    override func viewDidLoad() {
        super.viewDidLoad()
        // Register the user info table view cell
        self.tableView.register(UINib(nibName: "UserInfoTableViewCell", bundle: nil), forCellReuseIdentifier: "User Info Cell")
        // Locale
        NotificationCenter.default.addObserver(self, selector: #selector(onLanguageChange), name: NSNotification.Name(LCLLanguageChangeNotification), object: nil)
        onLanguageChange()
    }
    
    @objc func onLanguageChange() {
        title = "action_settings".localized()
        tableView.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if let savedLang = UserDefaults.standard.object(forKey: "pref_app_language") as? String {
            for i in 0 ..< SettingsTableViewController.langValues.count {
                if SettingsTableViewController.langValues[i] == savedLang {
                    tableView.cellForRow(at: IndexPath(row: i, section: 1))?.accessoryType = .checkmark
                }
            }
        }
        
        if let savedSize = UserDefaults.standard.object(forKey: "pref_text_size") as? Int {
            for i in 0 ..< SettingsTableViewController.sizeValues.count {
                if SettingsTableViewController.sizeValues[i] == savedSize {
                    tableView.cellForRow(at: IndexPath(row: i, section: 2))?.accessoryType = .checkmark
                }
            }
        }
    }
    
    @IBAction func onDone(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return SettingsTableViewController.sectionNames.count
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return SettingsTableViewController.sectionNames[section].localized()
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return indexPath.section == 0 ? 160 : 44
    }
    
    override func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        #if DEBUG
            if section == 3 {
                return "pref_footer_installation_id".localized()
            } else {
                return nil
            }
        #else
            return nil
        #endif
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return SettingsTableViewController.rowNames[section].count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = indexPath.section == 0 ? "User Info Cell" : "Cell"
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath)
        // Configure the cell...
        cell.textLabel?.text = SettingsTableViewController.rowNames[indexPath.section][indexPath.row]?.localized()
        // Special settings for about section
        if indexPath.section == 3 {
            switch indexPath.row {
            case 0:
                cell.detailTextLabel?.text = UserDefaults.standard.value(forKey: "pref_installation_id") as? String ?? nil
            case 1:
                cell.detailTextLabel?.text = String(UserDefaults.standard.integer(forKey: "pref_user_level"))
            default:
                break
            }
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // Remove all accessory on each section
        for i in 0 ..< tableView.numberOfRows(inSection: indexPath.section) {
            tableView.cellForRow(at: IndexPath(row: i, section: indexPath.section))?.accessoryType = .none
        }
        
        // Deselect selected
        tableView.deselectRow(at: indexPath, animated: false)
        
        switch indexPath.section {
        case 0:
            break
        case 1:
            // Store settings
            UserDefaults.standard.set(SettingsTableViewController.langValues[indexPath.row], forKey: "pref_app_language")
            UserDefaults.standard.synchronize()
            
            // Check the newly selected
            tableView.cellForRow(at: indexPath)?.accessoryType = .checkmark
            
            // Purge manager caches
            (UIApplication.shared.delegate as! AppDelegate).booksMgr?.purgeCache()
            (UIApplication.shared.delegate as! AppDelegate).gamesMgr?.purgeCache()
            PostCardManager.instance.purgeCache()
            (UIApplication.shared.delegate as! AppDelegate).pressureMgr?.purgeCache()
            
            // Finally, change display language
            Localize.setCurrentLanguage(SettingsTableViewController.langValues[indexPath.row])
        case 2:
            // Store settings
            UserDefaults.standard.set(SettingsTableViewController.sizeValues[indexPath.row], forKey: "pref_text_size")
            UserDefaults.standard.synchronize()
            
            // Check the newly selected
            tableView.cellForRow(at: indexPath)?.accessoryType = .checkmark
        case 3:
            // Nothing to do at the about section
            break
        default:
            NSLog("[\(TAG)] Unknown section \(indexPath.section) selected?")
        }
    }
}
