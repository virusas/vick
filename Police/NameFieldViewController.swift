//
//  NameFieldViewController.swift
//  Police
//
//  Created by Crossover on 19/6/2018.
//  Copyright © 2018年 Crossover. All rights reserved.
//

import UIKit

class NameFieldViewController: UIViewController, UITextViewDelegate {
    // MARK: - Properties
    
    @IBOutlet var NameField: UITextField!
    @IBOutlet var doneButton: UIBarButtonItem!
    
    // MARK: - View Life Cycle
    
    override func viewWillAppear(_ animated: Bool) {
        // Set navigation bar to translucent
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
    }
    
    // MARK: - UITextViewDelegate
    
    @objc func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.isEqual(NameField) {
            NameField.resignFirstResponder()
        }
        return true
    }
    
    // MARK: - Storyboard callbacks
    
    @IBAction func textFieldEditingChanged(_ sender: Any) {
        let senderField: UITextField = sender as! UITextField
        if senderField.isEqual(NameField) {
            doneButton.isEnabled = senderField.hasText
        }
    }
    
    @IBAction func nextPressed(_ sender: Any){
        let name = NameField.text
        UserDefaults().set(name, forKey: "userName")
        
        let storyboard = UIStoryboard(name: BookTableViewController.storyboardName, bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "WelcomeViewController")
        
        // Replace root view controller
        self.navigationController?.setViewControllers([viewController], animated: true)
    }
}

