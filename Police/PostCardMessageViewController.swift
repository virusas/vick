//
//  PostCardMessageViewController.swift
//  Police
//
//  Created by Crossover on 6/15/18.
//  Copyright © 2018 Crossover. All rights reserved.
//

import UIKit

class PostCardMessageViewController: UIViewController, UITextFieldDelegate {
    @IBOutlet weak var editHeaderTextButton: UIButton!
    @IBOutlet weak var editHeaderColorButton: UIButton!
    
    @IBOutlet weak var editMessageTextButton: UIButton!
    @IBOutlet weak var editMessageColorButton: UIButton!
    @IBOutlet weak var editDefaultTextButton: UIButton!
    
    @IBOutlet weak var editFooterTextButton: UIButton!
    @IBOutlet weak var editFooterColorButton: UIButton!
    
    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet weak var lblContent: UILabel!
    @IBOutlet weak var lblFooter: UILabel!
    
    @IBOutlet weak var txtHeader: UITextField!
    @IBOutlet weak var txtContent: UITextField!
    @IBOutlet weak var txtFooter: UITextField!
    
    @IBOutlet weak var getTemplate: UIButton!
    
    @IBOutlet weak var barItem: UIBarItem!
    
    
    private let TAG: String = "PostCardMessageViewController"

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarItem.title = "action_text".localized()
        // Do any additional setup after loading the view.
        
        lblHeader.text = "post_card_text_position_header".localized()
        lblContent.text = "post_card_text_position_message".localized()
        lblFooter.text = "post_card_text_position_footer".localized()
        
        if let header  : String = UserDefaults.standard.string(forKey: "post-card-header") {
            txtHeader.text = header as! String
        }
        if let content  : String  = UserDefaults.standard.string(forKey: "post-card-content") {
            txtContent.text = content as! String
        }
        if let footer  : String  = UserDefaults.standard.string(forKey: "post-card-footer") {
            txtFooter.text = footer as! String
        }
        txtHeader.delegate = self
txtContent.delegate = self
        txtFooter.delegate = self
    }

    @IBAction func onButtonClick(_ sender: UIButton) {
        /*
        switch sender {
        case editHeaderTextButton:
            return
        case editHeaderColorButton:
            return
        case editMessageTextButton:
            return
        case editMessageColorButton:
            return
        case editDefaultTextButton:
            return
        case editFooterTextButton:
            return
        case editFooterColorButton:
            return
        default:
            NSLog("[\(TAG)] onButtonClick() Received unknown button?")
        }*/
        
        var msg: String = "post_card_text_template_1".localized();
         msg = msg + "\n\n";
        
         msg += "post_card_text_template_2".localized();
         msg = msg + "\n\n";

         msg += "post_card_text_template_3".localized();
        msg = msg + "\n\n";

         msg += "post_card_text_template_4".localized();
        msg = msg + "\n\n";

         msg += "post_card_text_template_5".localized();
        msg = msg + "\n\n";

         msg += "post_card_text_template_6".localized();
        msg = msg + "\n\n";
        msg += "post_card_text_template_7".localized();

        let alertController = UIAlertController(title: "", message:msg, preferredStyle: .actionSheet)
        alertController.addAction(UIAlertAction(title: "Done", style: .default, handler: onTextChange))
        
        present(alertController, animated: true, completion: nil)
        
    }
    
    override func awakeFromNib(){
        
        self.tabBarItem.title = "action_text".localized()
    }
    
    
    
    // TODO: Can this detect what action its from
    private func onTextChange(alert: UIAlertAction!) {
        
    }
    
    private func createInputDialog() -> UIAlertController? {
        let alertController = UIAlertController(title: "Edit Text", message: nil, preferredStyle: .actionSheet)
        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        alertController.addAction(UIAlertAction(title: "Done", style: .default, handler: onTextChange))
        return alertController
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        NotificationCenter.default.post(name: .hideKB, object: nil)
        return false
    }
    
    @IBAction func textFieldDidStartEditing(textField: UITextField) {
        NotificationCenter.default.post(name: .showKB, object: textField.text)
    }
    @IBAction func textFieldDidEndEditing(textField: UITextField) {
        if (textField == txtHeader) {
            NotificationCenter.default.post(name: .header, object: textField.text)
        }
        else if (textField == txtContent) {
            NotificationCenter.default.post(name: .content, object: textField.text)
        }
        else  if (textField == txtFooter) {
            NotificationCenter.default.post(name: .footer, object: textField.text)
        }
    }
    
}
