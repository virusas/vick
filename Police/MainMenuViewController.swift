//
//  MainMenuViewController.swift
//  Police
//
//  Created by Aluminum on 7/21/17.
//  Copyright © 2017 Crossover. All rights reserved.
//

import UIKit
import Localize_Swift
import Floaty


class MainMenuViewController: BaseViewController {
    @IBOutlet var ebookButton: UIButton!
    @IBOutlet var antiLabelButton: UIButton!
    @IBOutlet var bookmarksButton: UIButton!
    @IBOutlet var gamesButton: UIButton!
    @IBOutlet var postcardButton: UIButton!
    @IBOutlet var pressureTestButton: UIButton!
    @IBOutlet var resourcesButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Locale
        NotificationCenter.default.addObserver(self, selector: #selector(onLanguageChange), name: NSNotification.Name(LCLLanguageChangeNotification), object: nil)
        onLanguageChange()
        let floaty = Floaty()
        var psyName = "psy_f"
        if (UserDefaults().string(forKey: "psyImg")) != nil {
            psyName = UserDefaults().string(forKey: "psyImg")!
        }
        floaty.addItem("action_resources".localized(), icon: UIImage(named: psyName), handler: { item in
            self.onClick(self.resourcesButton)
            floaty.close()
        })
        floaty.addItem("Help", icon: UIImage(named: psyName), handler: {item in
            let alert = UIAlertController(title: "app_tutorial".localized(), message: "tutorial3".localized(), preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "action_next".localized(), style: .default, handler: {item in
            let alert = UIAlertController(title: "tutorialTitle4".localized(), message: "tutorial4".localized(), preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "action_next".localized(), style: .default, handler: {item in
            let alert = UIAlertController(title: "tutorialTitle5".localized(), message: "tutorial5".localized(), preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "action_next".localized(), style: .default, handler: {item in
            let alert = UIAlertController(title: "tutorialTitle6".localized(), message: "tutorial6".localized(), preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "action_next".localized(), style: .default, handler: {item in
            let alert = UIAlertController(title: "tutorialTitle7".localized(), message: "tutorial7".localized(), preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "action_next".localized(), style: .default, handler: {item in
            let alert = UIAlertController(title: "tutorialTitle8".localized(), message: "tutorial8".localized(), preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "action_next".localized(), style: .default, handler: {item in
            let alert = UIAlertController(title: "tutorialTitle9".localized(), message: "tutorial9".localized(), preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "action_next".localized(), style: .default, handler: {item in
            let alert = UIAlertController(title: "tutorialTitle10".localized(), message: "tutorial10".localized(), preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "action_next".localized(), style: .default, handler: {item in
            let alert = UIAlertController(title: "app_tutorial".localized(), message: "tutorial11".localized(), preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "action_next".localized(), style: .default, handler: {item in
            let alert = UIAlertController(title: "app_tutorial".localized(), message: "tutorial12".localized(), preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "action_next".localized(), style: .default, handler: nil))
                        self.present(alert, animated: true, completion: nil)}))
                        self.present(alert, animated: true, completion: nil)}))
                        self.present(alert, animated: true, completion: nil)}))
                        self.present(alert, animated: true, completion: nil)}))
                        self.present(alert, animated: true, completion: nil)}))
                        self.present(alert, animated: true, completion: nil)}))
                        self.present(alert, animated: true, completion: nil)}))
                        self.present(alert, animated: true, completion: nil)}))
                        self.present(alert, animated: true, completion: nil)}))
                        self.present(alert, animated: true, completion: nil)})
                        self.view.addSubview(floaty)
            }
    
    @objc func onLanguageChange() {
        title = "action_main_menu".localized()
        
        // Localize all texts
        ebookButton.setTitle("action_books".localized(), for: UIControlState.normal)
        gamesButton.setTitle("action_games".localized(), for: UIControlState.normal)
        antiLabelButton.setTitle("action_anti_label".localized(), for: UIControlState.normal)
        bookmarksButton.setTitle("action_bookmarks".localized(), for: UIControlState.normal)
        postcardButton.setTitle("action_post_card".localized(), for: UIControlState.normal)
        pressureTestButton.setTitle("action_pressure_test".localized(), for: UIControlState.normal)
        resourcesButton.setTitle("action_resources".localized(), for: UIControlState.normal)
    }
    
    @IBAction func onClick(_ sender: Any) {
        if sender is UIButton {
            let senderButton: UIButton = sender as! UIButton
            
            var indexPath: IndexPath? = nil
            if senderButton.isEqual(ebookButton) {
                indexPath = IndexPath(row: 0, section: 1)
            } else if senderButton.isEqual(antiLabelButton) {
                indexPath = IndexPath(row: 3, section: 1)
            } else if senderButton.isEqual(bookmarksButton) {
                indexPath = IndexPath(row: 6, section: 1)
            } else if senderButton.isEqual(gamesButton) {
                indexPath = IndexPath(row: 2, section: 1)
                
            } else if senderButton.isEqual(postcardButton) {
                indexPath = IndexPath(row: 5, section: 1)
                
            } else if senderButton.isEqual(pressureTestButton) {
                indexPath = IndexPath(row: 1, section: 1)
            } else if senderButton.isEqual(resourcesButton) {
                indexPath = IndexPath(row: 4, section: 1)
            }
            
            if indexPath != nil {
                let drawer = self.navigationController?.parent as! KYDrawerController
                let drawerVC = drawer.drawerViewController?.childViewControllers[0] as! DrawerTableViewController
                drawerVC.changeFragment(indexPath: indexPath!)
            }
        }
    }
}
