//
//  GameStagesTableViewController.swift
//  Police
//
//  Created by Aluminum on 8/15/17.
//  Copyright © 2017 Crossover. All rights reserved.
//

import UIKit

class GameStagesTableViewController: UITableViewController {
    private var parentVC: UIViewController!
    private var gameMgr: GamesManager!
    
    class func newInstance(parentVC: UIViewController, popoverPresentControlDelegate: UIPopoverPresentationControllerDelegate, source: UIButton) -> GameStagesTableViewController? {
        let storyboard = UIStoryboard(name: BookTableViewController.storyboardName, bundle: nil)
        if let stageVC = storyboard.instantiateViewController(withIdentifier: "GameStagesTableViewController") as? GameStagesTableViewController {
            stageVC.parentVC = parentVC
            stageVC.modalPresentationStyle = .popover
            stageVC.popoverPresentationController?.delegate = popoverPresentControlDelegate
            stageVC.popoverPresentationController?.sourceView = source
            stageVC.popoverPresentationController?.sourceRect = source.bounds
            return stageVC
        } else {
            return nil
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        gameMgr = (UIApplication.shared.delegate as! AppDelegate).gamesMgr
        self.preferredContentSize.height = CGFloat((gameMgr.getStages()?.count ?? 3) * 44)
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return gameMgr.getStages()?.count ?? 0
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)

        // Configure the cell...
        let info = gameMgr.getStage(indexPath.row)
        if info != nil {
            cell.textLabel?.text = String.init(format: "#%d - %@", indexPath.row + 1, info!.mTitle)
            cell.textLabel?.textColor = UIColor.black
            if info!.mCompleted {
                cell.accessoryType = .checkmark
            } else if (indexPath.row >= gameMgr.getProgress()) {
                cell.textLabel?.textColor = UIColor.gray
            }
        }

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        
        // Start the stage
        if let stageInfo = gameMgr.getStage(indexPath.row) {
            if let gameVC = GameViewController.gameViewControllerForStage(parentVC, stageInfo) {
                self.dismiss(animated: false, completion: {
                    self.parentVC.present(gameVC, animated: true, completion: nil)
                })
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        return indexPath.row < gameMgr.getProgress()
    }
}
