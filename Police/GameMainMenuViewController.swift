//
//  GameMainMenuViewController.swift
//  Police
//
//  Created by Aluminum on 8/7/17.
//  Copyright © 2017 Crossover. All rights reserved.
//

import UIKit
import Localize_Swift

class GameMainMenuViewController: BaseViewController, UIPopoverPresentationControllerDelegate {
    @IBOutlet var gameTitleLabel: UILabel!
    @IBOutlet weak var gameTagLineLabel: UILabel!
    @IBOutlet var startButton: UIButton!
    @IBOutlet var aboutButton: UIButton!
    
    var gameMgr: GamesManager!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        gameMgr = (UIApplication.shared.delegate as! AppDelegate).gamesMgr

        // Locale
        NotificationCenter.default.addObserver(self, selector: #selector(onLanguageChange), name: NSNotification.Name(LCLLanguageChangeNotification), object: nil)
        onLanguageChange()
    }
    
    @objc func onLanguageChange() {
        // Localize all texts
        gameTitleLabel.text = gameMgr.getTitle()
        gameTitleLabel.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        gameTagLineLabel.text = gameMgr.getTagLine()
        gameTagLineLabel.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        startButton.setTitle("action_start_game".localized(), for: UIControlState.normal)
        aboutButton.setTitle("action_about_game".localized(), for: UIControlState.normal)
    }
    
    @IBAction func onButtonClicked(_ sender: UIButton) {
        switch (sender) {
        case startButton:
            if let stageVC = GameStagesTableViewController.newInstance(parentVC: self, popoverPresentControlDelegate: self, source: startButton) {
                self.present(stageVC, animated: true, completion: nil)
            }
        case aboutButton:
            let alert = UIAlertController(title: "action_about_game".localized(), message: "action_about_message".localized(), preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        default:
            return
        }
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        // Only GameStagesTableViewController will call this
        return .none
    }
}
