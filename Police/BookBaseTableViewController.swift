//
//  BookTableViewController.swift
//  Police
//
//  Created by Aluminum on 7/25/17.
//  Copyright © 2017 Crossover. All rights reserved.
//

import UIKit

class BookTableViewCell: UITableViewCell {
    @IBOutlet var chapterLabel: UILabel!
    @IBOutlet var titleLabel: UILabel?
    @IBOutlet var imagelabel: UIImageView!
}

class BookBaseTableViewController: BaseTableViewController {
    static let nibName: String = "BookTableCell"
    static let tableViewCellIdentifier = "Book Table Cell"

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.estimatedRowHeight = CGFloat(75)
        tableView.rowHeight = UITableViewAutomaticDimension

        // Required if our subclasses are to use `dequeueReusableCellWithIdentifier(_:forIndexPath:)`.
        let nib = UINib(nibName: BookTableViewController.nibName, bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: BookTableViewController.tableViewCellIdentifier)
        
    }
    
    func configureCell(_ cell: BookTableViewCell, forChapter chapter: ChapterInfo) {
        cell.chapterLabel.text = String(format: "chapter_row_item".localized(), arguments: [chapter.mNum])
        cell.titleLabel?.text = chapter.mTitle
        cell.imagelabel?.image = UIImage(named:chapter.mImage)
    }
}
