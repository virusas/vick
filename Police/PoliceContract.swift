//
//  PoliceContract.swift
//  Police
//
//  Created by Aluminum on 8/2/17.
//  Copyright © 2017 Crossover. All rights reserved.
//

import Foundation

class PoliceContract {
    // iOS - Added for db version support
    class DatabaseHeader {
        static let TABLE_NAME: String = "db_header"
        static let COLUMN_NAME_VERSION = "version"
    }
    
    class BookmarkEntry {
        static let TABLE_NAME: String = "bookmarks"
        static let COLUMN_NAME_BOOK: String = "book"
        static let COLUMN_NAME_CHAPTER: String = "chapter"
        static let COLUMN_NAME_CHAPTER_INDEX: String = "chapterIndex"
        static let COLUMN_NAME_IMAGE: String = "image"
    }
    
    class GameRecordEntry {
        static let TABLE_NAME: String = "game_records"
        static let COLUMN_NAME_CODENAME = "codename"
        static let COLUMN_NAME_VERSION = "version"
        static let COLUMN_NAME_COMPLETED = "completed"
        static let COLUMN_NAME_COMPLETION_DATE = "completionDate"
    }
    
    class PressureTestEntry{
        static let TABLE_NAME: String = "pressure_test"
        static let COLUMN_NAME_DATE = "dateTaken"
        static let COLUMN_NAME_MARK = "mark"
    }
}
