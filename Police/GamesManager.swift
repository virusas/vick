//
//  GamesManager.swift
//  Police
//
//  Created by Aluminum on 8/15/17.
//  Copyright © 2017 Crossover. All rights reserved.
//

import Foundation
import Localize_Swift

class StageInfo {
    let mNum: Int
    let mTitle, mVersion, mFilePath: String
    var mSteps: [StepInfo]
    
    var mCompleted: Bool! = false
    var mLevelUp: Bool! = true
    
    init(_ num: Int, _ title: String, _ version: String, _ filePath: String) {
        mNum = num
        mTitle = title
        mVersion = version
        mFilePath = filePath
        mSteps = [StepInfo]()
    }
}

class StepInfo {
    let mType: Int
    let mLocation, mBackgroundImage: String?
    
    init(_ type: Int, _ location: String?, _ backgroundImage: String?) {
        mType = type
        mLocation = location
        mBackgroundImage = backgroundImage
    }
}

class MessageInfo: StepInfo {
    let mSpeaker, mMessage: String
    
    init(_ location: String?, _ speaker: String, _ message: String, _ backgroundImg: String?) {
        mSpeaker = speaker
        mMessage = message
        super.init(0, location, backgroundImg)
    }
}

class QuestionInfo: StepInfo {
    let mSpeaker,mQuestion: String
    let mHint: HintInfo?
    var mAnswers: [AnswerInfo]
    
    init(_ location: String?, _ speaker: String, _ question: String, _ backgroundImg: String?, _ hint: HintInfo?) {
        mSpeaker = speaker
        mQuestion = question
        mHint = hint
        mAnswers = [AnswerInfo]()
        super.init(1, location, backgroundImg)
    }
}

struct HintInfo {
    let mBookName: String
    let mChapter, mChapterIndex: Int
    //let mImage: String
}

extension HintInfo {
    init?(json: [String: Any]) {
        guard let bookName = json["bookName"] as? String,
            let chapter = json["title"] as? String,
            let chapterIndex = json["chapter"] as? String
            //,
            //let image = json["image"] as? String
            else {
                return nil
        }
        
        guard let chapterInt = Int(chapter),
            let chapterIndexInt = Int(chapterIndex)
            else {
                return nil
        }
        
        mBookName = bookName
        mChapter = chapterInt
        mChapterIndex = chapterIndexInt
        //mImage = image
    }
}

class AnswerInfo {
    let mAnswer: String
    let mCorrect: Bool
    
    init(_ answer: String, _ correct: Bool) {
        mAnswer = answer
        mCorrect = correct
    }
}

class GamesManager {
    private let TAG: String = "GamesManager"
    
    private var mTitle, mTagLine: String!
    private var mStages: [StageInfo]!
    private var mProgress: Int
    
    public init() {
        mProgress = 1
        purgeCache()
    }
    
    public func purgeCache() {
        mTitle = nil
        if mStages != nil {
            mStages.removeAll()
            mStages = nil
        }
        mProgress = 1
    }
    
    public func getTitle() -> String? {
        if mTitle == nil {
            buildGameInfo()
        }
        
        return mTitle
    }
    
    public func getTagLine() -> String? {
        if mTagLine == nil{
            buildGameInfo()
        }
        
        return mTagLine
    }
    
    public func getStages() -> [StageInfo]? {
        if mStages == nil {
            buildGameInfo()
        }
        
        // Update the state first
        updateRecord()
        
        return mStages.count > 0 ? mStages : nil
    }
    
    public func getStage(_ codename: String) -> StageInfo? {
        if mStages == nil {
            buildGameInfo()
        }
        
        // Update the state first
        updateRecord()
        
        for stage in mStages {
            if stage.mFilePath == codename {
                return stage
            }
        }
        return nil
    }
    
    public func getStage(_ index: Int) -> StageInfo? {
        if mStages == nil {
            buildGameInfo()
        }
        
        // Update the state first
        updateRecord()
        
        return (index >= 0 && mStages.count > index) ? mStages[index] : nil
    }
    
    public func getProgress() -> Int {
        if mStages == nil {
            buildGameInfo()
        }
        
        // Update the state first
        updateRecord()
        
        return mProgress
    }
    
    public func buildMessage(_ jsonMessage: [String: Any], _ locale: String) -> MessageInfo? {
        let speaker = (jsonMessage["speaker"] as! [String: Any])[locale] as! String
        let message = (jsonMessage["message"] as! [String: Any])[locale] as! String
        var location: String? = nil
        if let data = (jsonMessage["location"] as? [String: Any])?[locale] as? String {
            location = data
        }
        var backgroundImage: String? = nil
        if let data = jsonMessage["background_image"] as? String {
            backgroundImage = data
        }

        return MessageInfo(location, speaker, message, backgroundImage)
    }
    
    public func buildAnswersForQuestion(_ question: QuestionInfo, _ answers: [[String: Any]], _ locale: String) {
        for i in 0 ..< answers.count {
            let answer = answers[i][locale] as! String
            var correct: Bool = false
            if let data = answers[i]["correct"] as? Bool {
                correct = data
            }
            
            let answerInfo = AnswerInfo(answer, correct)
            question.mAnswers.append(answerInfo)
        }
    }
    
    public func buildQuestion(_ jsonQuestion: [String: Any], _ locale: String) -> QuestionInfo? {
        let speaker = (jsonQuestion["speaker"] as! [String: Any])[locale] as! String
        let question = (jsonQuestion["question"] as! [String: Any])[locale] as! String
        var location: String? = nil
        if let data = (jsonQuestion["location"] as? [String: Any])?[locale] as? String {
            location = data
        }
        var backgroundImage: String? = nil
        if let data = jsonQuestion["background_image"] as? String {
            backgroundImage = data
        }

        var hint: HintInfo? = nil
        if let data = jsonQuestion["ebook"] as? [String: Any] {
            hint = HintInfo(json: data)
        }
        
        let questionInfo = QuestionInfo(location, speaker, question, backgroundImage, hint)
        buildAnswersForQuestion(questionInfo, (jsonQuestion["answers"] as! [[String: Any]]), locale)
        return questionInfo
    }
    
    public func buildSteps(_ info: StageInfo, _ locale: String) {
        let jsonScene = JSONUtils.readJSONArray("games/\(info.mFilePath)/scene", "json")
        if jsonScene == nil || jsonScene!.count <= 0 {
            return
        }
        
        for i in 0 ..< jsonScene!.count {
            let step = jsonScene![i]
            if let type = step["type"] as? Int {
                switch type {
                case 0:
                    if let message = buildMessage(step, locale) {
                        info.mSteps.append(message)
                    }
                case 1:
                    if let question = buildQuestion(step, locale) {
                        info.mSteps.append(question)
                    }
                default:
                    NSLog("[\(TAG)] Unknown scene step type \(type)")
                }
            }
        }
    }
    
    public func buildStages(_ stages: [String], _ locale: String) {
        if stages.count <= 0 {
            return
        }
        
        mStages = [StageInfo]()
        for i in 0 ..< stages.count {
            let filePath = stages[i]
            let gameInfo = JSONUtils.readJSONObject("games/\(filePath)/info", "json")
            if gameInfo == nil {
                continue
            }
            
            let title = (gameInfo!["title"] as! [String: Any])[locale] as! String
            let version = gameInfo!["version"] as! String
            
            let info = StageInfo(i, title, version, filePath)
            buildSteps(info, locale)
            mStages.append(info)
        }
    }
    
    public func buildGameInfo() {
        // Get locale settting
        var locale = Localize.currentLanguage()
       // if locale == "zh-Hant" {
            // We need to warp this to zh-tw to cope with assets
            locale = "zh-tw"
        //}
        
        let jsonInfo = JSONUtils.readJSONObject("games/info", "json")
        if jsonInfo == nil {
            return
        }
        
        mTitle = (jsonInfo!["title"] as! [String: Any])[locale] as? String
        mTagLine = ((jsonInfo!["tagLine"] as! [String: Any])[locale] as! String)
        
        let jsonStages = jsonInfo!["stages"] as! [String]
        buildStages(jsonStages, locale)
    }
    
    private func updateRecord() {
        if mStages.count <= 0 {
            return
        }
        
        mProgress = 1
        var dbHelper = PoliceDBHelper()
        for stage in mStages {
            let query = "SELECT \(PoliceContract.GameRecordEntry.COLUMN_NAME_CODENAME), \(PoliceContract.GameRecordEntry.COLUMN_NAME_VERSION), \(PoliceContract.GameRecordEntry.COLUMN_NAME_COMPLETED), \(PoliceContract.GameRecordEntry.COLUMN_NAME_COMPLETION_DATE) " +
                "FROM \(PoliceContract.GameRecordEntry.TABLE_NAME) " +
                "WHERE \(PoliceContract.GameRecordEntry.COLUMN_NAME_CODENAME) = '\(stage.mFilePath)' AND \(PoliceContract.GameRecordEntry.COLUMN_NAME_VERSION) = '\(stage.mVersion)' " +
                "LIMIT 1"

            let statement = dbHelper?.query(query)

            if sqlite3_step(statement) == SQLITE_ROW {
                stage.mCompleted = Int(sqlite3_column_int(statement, 2)) == 1
                if stage.mCompleted {
                    mProgress += 1
                    stage.mLevelUp = true
                }
            }
            
            sqlite3_finalize(statement)
        }
        dbHelper = nil
    }
}
