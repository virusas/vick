//
//  PostCardImageViewController.swift
//  Police
//
//  Created by Crossover on 6/28/18.
//  Copyright © 2018 Crossover. All rights reserved.
//

import UIKit

private let reuseIdentifier = "ImageCell"
private let itemsPerRow: CGFloat = 4
private let sectionInsets = UIEdgeInsets(top: 4.0, left: 4.0, bottom: 4.0, right: 4.0)


class PostCardImageViewController: UICollectionViewController{

    private var dataSource: [String]?
    private var mainView = PostCardMainViewController()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tabBarItem.title = "action_image".localized()
        // Preserve selection between presentations
        self.clearsSelectionOnViewWillAppear = false
        
        // Gather list of images
        self.dataSource = PostCardManager.instance.getImages()
        
        if let id : Int = UserDefaults.standard.integer(forKey: "post-card-id"){
            let indexPath = IndexPath(row: id, section: 0)
            let descript = indexPath.row.description;
            if let fileName = self.dataSource?[indexPath.row] {
                if let image = UIImage(named: "post_card/images/\(fileName)", in: Bundle.main, compatibleWith: nil) {
                    NotificationCenter.default.post(name: .image, object: image)
                }
            }
            self.collectionView?.selectItem(at: indexPath, animated: false, scrollPosition: UICollectionViewScrollPosition.centeredVertically)
        }
    }
    
    override func awakeFromNib(){
        
        self.tabBarItem.title = "action_image".localized()
    }
    

    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return section == 0 ? dataSource?.count ?? 0 : 0
    }
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let cell : PostCardImageCell =  collectionView.cellForItem(at: indexPath) as! PostCardImageCell{
            
            //DispatchQueue.main.async {
            //    let postCardMainViewController = PostCardMainViewController()
            //    postCardMainViewController.setPreviewImage(imgId:indexPath.row)
            //}
            //let img: UIImage = UIImage(named: String(indexPath.row)) ?? UIImage.init()
            //postCardDelegate?.changeImage(img)
            
            let descript = indexPath.row.description;
            if let fileName = self.dataSource?[indexPath.row] {
                if let image = UIImage(named: "post_card/images/\(fileName)", in: Bundle.main, compatibleWith: nil) {
                    DispatchQueue.main.async {
                        NotificationCenter.default.post(name: .image, object: image)
                        NotificationCenter.default.post(name: .id, object: indexPath.row)
                    }
                }
            }
            
        }
    }
    
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! PostCardImageCell
        
        // Configure the cell
        DispatchQueue.global().async {
            // Make sure we have data source
            if let fileName = self.dataSource?[indexPath.row] {
                if let image = UIImage(named: "post_card/images/\(fileName)", in: Bundle.main, compatibleWith: nil) {
                    DispatchQueue.main.async {
                        cell.imageView?.image = image
                    }
                }
            } else {
                DispatchQueue.main.async {
                    cell.backgroundColor = UIColor.white
                }
            }
        }
    return cell
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: UICollectionViewDelegate

    
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
        return true
    }
  

}


extension PostCardImageViewController: UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let itemPadding = sectionInsets.left * (itemsPerRow + 1)
        let frameWidth = view.frame.width - itemPadding
        
        // We are always square
        let itemLength = frameWidth / itemsPerRow
        return CGSize(width: itemLength, height: itemLength)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return sectionInsets.right
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return sectionInsets.left
    }
    

}
