//
//  GameMessageViewController.swift
//  Police
//
//  Created by Aluminum on 8/16/17.
//  Copyright © 2017 Crossover. All rights reserved.
//

import UIKit
import Localize_Swift

class GameMessageViewController: UIViewController {
    private let TAG: String = "GameMessageViewController"
    
    @IBOutlet var speakerLabel: UILabel!
    @IBOutlet var messageLabel: UILabel!
    @IBOutlet var nextButton: UIButton!
    
    var message: MessageInfo!
    
    class func gameMessageViewControllerForStep(_ messageInfo: MessageInfo) -> GameMessageViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "GameMessageViewController") as! GameMessageViewController
        viewController.message = messageInfo
        return viewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        speakerLabel.text = message.mSpeaker
        messageLabel.text = message.mMessage
        
        // Locale
        NotificationCenter.default.addObserver(self, selector: #selector(onLanguageChange), name: NSNotification.Name(LCLLanguageChangeNotification), object: nil)
        onLanguageChange()
    }
    
    @objc func onLanguageChange() {
        nextButton.setTitle("action_next".localized(), for: .normal)
    }
    
    @IBAction func onNextPressed(_ sender: UIButton) {
        let viewController = parent as? GameViewController
        if viewController == nil {
            NSLog("[\(TAG)] Unable to cast parent view controller to GameViewController")
            return
        }
        
        viewController!.nextStep()
    }
}
