//
//  CharacterTableViewController.swift
//  Police
//
//  Created by Crossover on 15/6/2018.
//  Copyright © 2018年 Crossover. All rights reserved.
//

import UIKit

struct cellData {
    let text: String
    let image: UIImage!
}

var selection = 1

class CharacterTableViewController: UITableViewController {
    // Constants for Storyboard/ViewControllers
    static let storyboardName = "Main"
    static let viewControllerIdentifier = "CharacterTableViewController"
    // Table data
    var info = [cellData]()
    var currentTable: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Start your code
        UserDefaults().set(selection, forKey: "Selection")
    }
    
    override func tableView(_ tableView: UITableView,
                            numberOfRowsInSection section: Int) -> Int {
        return info.count
    }
    
    // create a cell for each table view row
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = Bundle.main.loadNibNamed("CharacterTableCell", owner: self, options: nil)?.first as! CharacterTableCell
        cell.myImg.image = info[indexPath.row].image
        cell.myLabel.text = info[indexPath.row].text
        
        return cell
    }
    
    // method to run when table view cell is tapped
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if currentTable == "pc" {
            if indexPath.row == 0{
                let image = "pc_m"
                UserDefaults().set(image, forKey: "userImg")
                print("You tapped cell \(image)")
            }
            else if indexPath.row == 1{
                let image = "pc_f"
                UserDefaults().set(image, forKey: "userImg")
                print("You tapped cell \(image)")
            }
            else {
                let image = "pc_m"
                UserDefaults().set(image, forKey: "userImg")
                print("You tapped cell \(image)")
            }
        }
        else if currentTable == "psy" {
            if indexPath.row == 0{
                let image = "psy_m"
                UserDefaults().set(image, forKey: "psyImg")
                print("You tapped cell \(image)")
            }
            else if indexPath.row == 1{
                let image = "psy_f"
                UserDefaults().set(image, forKey: "psyImg")
                print("You tapped cell \(image)")
            }
            else {
                let image = "psy_m"
                UserDefaults().set(image, forKey: "psyImg")
                print("You tapped cell \(image)")
            }
        }
        else {
            print("Error getting the table")
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        selection = 2
        UserDefaults().set(selection, forKey: "Selection")
    }
}
