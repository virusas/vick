//
//  ChapterTableViewCell.swift
//  Police
//
//  Created by Aluminum on 7/25/17.
//  Copyright © 2017 Crossover. All rights reserved.
//

import UIKit

class ChapterTableViewCell: UITableViewCell {
    @IBOutlet var titleLabel: UILabel!
}

class ChapterSummaryTableViewCell: UITableViewCell {
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var summaryLabel: UILabel!
}

class ChapterBaseTableViewController: UITableViewController {
    static let nibName: String = "ChapterTableCell"
    static let tableViewCellIdentifier = "Chapter Table Cell"
    
    static let nibNameSummary: String = "ChapterSummaryTableCell"
    static let tableViewCellIdentifierSummary = "Chapter Summary Table Cell"
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.estimatedRowHeight = CGFloat(44)
        tableView.rowHeight = UITableViewAutomaticDimension

        // Required if our subclasses are to use `dequeueReusableCellWithIdentifier(_:forIndexPath:)`.
        var nib = UINib(nibName: ChapterTableViewController.nibName, bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: ChapterTableViewController.tableViewCellIdentifier)
        nib = UINib(nibName: ChapterTableViewController.nibNameSummary, bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: ChapterTableViewController.tableViewCellIdentifierSummary)
    }

    func configureCell(_ cell: ChapterTableViewCell, forChapterIndex chapterIndex: ChapterIndexInfo) {
        cell.titleLabel.text = chapterIndex.mTitle
    }
    
    func configureCellSummary(_ cell: ChapterSummaryTableViewCell, forChapterIndex chapterIndex: ChapterIndexInfo) {
        cell.titleLabel.text = chapterIndex.mTitle
        //cell.summaryLabel.text = chapterIndex.mSummary
    }
}
