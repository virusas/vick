//
//  PostCardImageCell.swift
//  Police
//
//  Created by Crossover on 6/28/18.
//  Copyright © 2018 Crossover. All rights reserved.
//

import UIKit

class PostCardImageCell: UICollectionViewCell{
    var imageView : UIImageView?
    
    override var isSelected: Bool {
        didSet {
            imageView?.layer.borderWidth = isSelected ? 4 : 0
        }
    }
    public override init(frame: CGRect) {
        super.init(frame: frame)
        self.initialize()
        self.createIv()
        }
        
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.initialize()
        }
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Apply border color
        imageView?.layer.borderColor = tintColor.cgColor
        // Make sure we are not selected
        isSelected = false
        
    }
    open override func prepareForReuse() {
        super.prepareForReuse()
        
        self.reset()
    }
    public func createIv() -> Void {
        imageView = UIImageView.init(frame:CGRect.init(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.height))
        self.addSubview(imageView!)
    }
    // MARK: - API
    
    open func initialize() {
        
    }
    
    open func reset() {
        
    }
}
