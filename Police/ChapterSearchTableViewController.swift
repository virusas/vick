//
//  ChapterSearchTableViewController.swift
//  Police
//
//  Created by Aluminum on 7/25/17.
//  Copyright © 2017 Crossover. All rights reserved.
//

import UIKit

class ChapterSearchTableViewController: ChapterBaseTableViewController {
    var filteredChapterIndexes = [ChapterIndexInfo]()
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredChapterIndexes.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ChapterBaseTableViewController.tableViewCellIdentifier, for: indexPath)
        
        // Configure the cell...
        let chapterIndex = filteredChapterIndexes[indexPath.row]
        configureCell(cell as! ChapterTableViewCell, forChapterIndex: chapterIndex)
        return cell
    }
}
