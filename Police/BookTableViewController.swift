//
//  BookTableViewController.swift
//  Police
//
//  Created by Aluminum on 7/25/17.
//  Copyright © 2017 Crossover. All rights reserved.
//

import UIKit
import Localize_Swift


class BookTableViewController: BookBaseTableViewController, UISearchBarDelegate, UISearchResultsUpdating {
    
    // MARK: - Types
    
    private let TAG: String = "BookTableViewController"
    
    // Constants for Storyboard/ViewControllers.
    static let storyboardName = "Main"
    static let viewControllerIdentifier = "BookTableViewController"
    
    // MARK: - Properties
    
    var searchController: UISearchController!
    var resultsTableController: BookSearchTableViewController!
    
    var bookName: String!
    var bookInfo: BookInfo!
    var image: String!
    
    // MARK: - Initialization
    
    class func bookTableViewControllerForBook(_ bookName: String) -> BookTableViewController {
        let storyboard = UIStoryboard(name: BookTableViewController.storyboardName, bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: BookTableViewController.viewControllerIdentifier) as! BookTableViewController
        
        // Configure view controller
        viewController.bookName = bookName
        
        return viewController
    }
    
    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Setup search result view
        resultsTableController = BookSearchTableViewController()
        
        // We want ourselves to be the delegate for this filtered table so didSelectRowAtIndexPath(_:) is called for both tables.
        resultsTableController.tableView.delegate = self
        
        searchController = UISearchController(searchResultsController: resultsTableController)
        searchController.searchResultsUpdater = self
        searchController.searchBar.sizeToFit()
        onLanguageChange()
        tableView.tableHeaderView = searchController.searchBar
        
        searchController.dimsBackgroundDuringPresentation = false // default is YES
        searchController.searchBar.delegate = self    // so we can monitor text changes + others
        
        /*
         Search is now just presenting a view controller. As such, normal view controller
         presentation semantics apply. Namely that presentation will walk up the view controller
         hierarchy until it finds the root view controller or one that defines a presentation context.
         */
        definesPresentationContext = true
        
        // Locale
        NotificationCenter.default.addObserver(self, selector: #selector(onLanguageChange), name: NSNotification.Name(LCLLanguageChangeNotification), object: nil)
    }
   
    deinit {
        searchController?.view.removeFromSuperview()
    }
    
    @objc func onLanguageChange() {
            searchController.searchBar.placeholder = "action_search_chapters".localized()
            searchController.searchBar.setValue("action_cancel".localized(), forKey:"_cancelButtonText")
            
            let booksMgr = (UIApplication.shared.delegate as! AppDelegate).booksMgr
            bookInfo = booksMgr?.getBook(bookName)!
            tableView.reloadData()
        
    }
    
    // MARK: - UISearchBarDelegate
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    // MARK: - UISearchResultsUpdating
    
    func updateSearchResults(for searchController: UISearchController) {
        // Update the filtered array based on the search text.
        let searchResults = bookInfo.mChapters
        
        // Strip out all the leading and trailing spaces.
        let whitespaceCharacterSet = CharacterSet.whitespaces
        let strippedString = searchController.searchBar.text!.trimmingCharacters(in: whitespaceCharacterSet)
        let searchItems = strippedString.components(separatedBy: " ") as [String]
        
        // Build all the "AND" expressions for each value in the searchString.
        let andMatchPredicates: [NSPredicate] = searchItems.map { searchString in
            var searchItemsPredicate = [NSPredicate]()
            
            // Below we use NSExpression represent expressions in our predicates.
            // NSPredicate is made up of smaller, atomic parts: two NSExpressions (a left-hand value and a right-hand value).
            let searchStringExpression = NSExpression(forConstantValue: searchString)
            
            // Name field matching.
            let titleExpression = NSExpression(forKeyPath: "mTitle")
            let titleSearchComparisonPredicate = NSComparisonPredicate(leftExpression: titleExpression, rightExpression: searchStringExpression, modifier: .direct, type: .contains, options: .caseInsensitive)
            
            searchItemsPredicate.append(titleSearchComparisonPredicate)
            
            //image
            let imageExpression = NSExpression(forKeyPath: "mImage")
            let imageSearchComparisonPredicate = NSComparisonPredicate(leftExpression: imageExpression, rightExpression: searchStringExpression, modifier: .direct, type: .contains, options: .caseInsensitive)
            
            searchItemsPredicate.append(imageSearchComparisonPredicate)
            
            
            // Summary field matching.
            /*let summaryExpression = NSExpression(forKeyPath: "mSummary")
            let summarySearchComparisonPredicate = NSComparisonPredicate(leftExpression: summaryExpression, rightExpression: searchStringExpression, modifier: .direct, type: .contains, options: .caseInsensitive)
            
            searchItemsPredicate.append(summarySearchComparisonPredicate)*/
            
            let numberFormatter = NumberFormatter()
            numberFormatter.numberStyle = .none
            numberFormatter.formatterBehavior = .default
            
            let targetNumber = numberFormatter.number(from: searchString)
            
            // `searchString` may fail to convert to a number.
            if targetNumber != nil {
                // Use `targetNumberExpression` in both the following predicates.
                let targetNumberExpression = NSExpression(forConstantValue: targetNumber!)
                
                // Num field matching.
                let numExpression = NSExpression(forKeyPath: "mNum")
                let numPredicate = NSComparisonPredicate(leftExpression: numExpression, rightExpression: targetNumberExpression, modifier: .direct, type: .equalTo, options: .caseInsensitive)
                
                searchItemsPredicate.append(numPredicate)
            }
            
            // Add this OR predicate to our master AND predicate.
            let orMatchPredicate = NSCompoundPredicate(orPredicateWithSubpredicates:searchItemsPredicate)
            
            return orMatchPredicate
        }
        
        // Match up the fields of the Product object.
        let finalCompoundPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: andMatchPredicates)
        
        let filteredResults = searchResults.filter { finalCompoundPredicate.evaluate(with: $0) }
        
        // Hand over the filtered results to our search results table.
        let resultsController = searchController.searchResultsController as! BookSearchTableViewController
        resultsController.filteredChapters = filteredResults
        resultsController.tableView.reloadData()
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return bookInfo.mChapters.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: BookTableViewController.tableViewCellIdentifier, for: indexPath)
        
        // Configure the cell...
        let chapter = bookInfo.getChapter(indexPath.row)
        configureCell(cell as! BookTableViewCell, forChapter: chapter! )
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selected: ChapterInfo
        
        // Check to see which table view cell was selected.
        if tableView === self.tableView {
            selected = bookInfo.getChapter(indexPath.row)!
        } else {
            selected = resultsTableController.filteredChapters[indexPath.row]
        }
        
        // Set up the book reader view controller to show.
        let viewController = ChapterTableViewController.chapterTableViewControllerForChapter(bookName, selected)
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}
