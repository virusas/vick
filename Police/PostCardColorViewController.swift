//
//  PostCardMessageViewController.swift
//  Police
//
//  Created by Crossover on 6/15/18.
//  Copyright © 2018 Crossover. All rights reserved.
//

import UIKit

class PostCardColorViewController: UIViewController, UITextFieldDelegate , HSBColorPickerDelegate{
    
    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet weak var lblContent: UILabel!
    @IBOutlet weak var lblFooter: UILabel!
    
    @IBOutlet weak var colorPickerView: UIView!
    @IBOutlet weak var selectSectionView: UIView!
    @IBOutlet weak var lblColorPicker: UILabel!
    @IBOutlet weak var colorPicker: HSBColorPicker!
    
    @IBOutlet weak var btnHeader: UIButton!
    @IBOutlet weak var btnContent: UIButton!
    @IBOutlet weak var btnFooter: UIButton!
    

    var selectedType : Int!
    
    struct type {
        static let header = 0
        static let content =  1
        static let footer =  2
    }
    
    
    private let TAG: String = "PostCardButtonViewController"

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        lblHeader.text = "post_card_text_position_header".localized()
        lblContent.text = "post_card_text_position_message".localized()
        lblFooter.text = "post_card_text_position_footer".localized()
        
        btnFooter.setTitle("post_card_change_color".localized(), for: UIControlState.normal)
        btnHeader.setTitle("post_card_change_color".localized(), for: UIControlState.normal)
        btnContent.setTitle("post_card_change_color".localized(), for: UIControlState.normal)
        lblColorPicker.text = "post_card_select_color".localized()
    }

    override func awakeFromNib(){
        self.tabBarItem.title = "action_color".localized()
    }

    
    
    @IBAction func buttonClick(btn: UIButton) {
        
        
        if (btn == btnHeader) {
            selectSectionView.isHidden = true
            colorPickerView.isHidden = false
            //NotificationCenter.default.post(name: .headerColor, object:)
            selectedType = type.header
            colorPicker.delegate = self
        }
        else if (btn == btnContent) {
            selectSectionView.isHidden = true
            colorPickerView.isHidden = false
            selectedType = type.content
            colorPicker.delegate = self

            //NotificationCenter.default.post(name: .contentcolor, object: )
        }
        else  if (btn == btnFooter) {
            selectSectionView.isHidden = true
            colorPickerView.isHidden = false
            selectedType = type.footer
            colorPicker.delegate = self
            
            //NotificationCenter.default.post(name: .footerColor, object:)
        }
        
    }
    
    func HSBColorColorPickerTouched(sender:HSBColorPicker, color:UIColor, point:CGPoint, state:UIGestureRecognizerState){
        if(selectedType == type.header){
            NotificationCenter.default.post(name: .headerColor, object:color)
            selectSectionView.isHidden = false
            colorPickerView.isHidden = true
        }else if(selectedType == type.content){
            NotificationCenter.default.post(name: .contentColor, object:color)
            selectSectionView.isHidden = false
            colorPickerView.isHidden = true
        }else if(selectedType == type.footer){
            NotificationCenter.default.post(name: .footerColor, object:color)
            selectSectionView.isHidden = false
            colorPickerView.isHidden = true
        }
    }
}



internal protocol HSBColorPickerDelegate : NSObjectProtocol {
    func HSBColorColorPickerTouched(sender:HSBColorPicker, color:UIColor, point:CGPoint, state:UIGestureRecognizerState)
}

@IBDesignable
class HSBColorPicker : UIView {
    
    weak  var delegate: HSBColorPickerDelegate?
    let saturationExponentTop:Float = 2.0
    let saturationExponentBottom:Float = 1.3
    
    @IBInspectable var elementSize: CGFloat = 1.0 {
        didSet {
            setNeedsDisplay()
        }
    }
    
    
    private func initialize() {
        
        self.clipsToBounds = true
        let touchGesture = UILongPressGestureRecognizer(target: self, action: #selector(self.touchedColor(gestureRecognizer:)))
        touchGesture.minimumPressDuration = 0
        touchGesture.allowableMovement = CGFloat.greatestFiniteMagnitude
        self.addGestureRecognizer(touchGesture)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialize()
    }
    
    override func draw(_ rect: CGRect) {
        let context = UIGraphicsGetCurrentContext()
        
        for y in stride(from: (0 as CGFloat), to: rect.height, by: elementSize) {
            
            var saturation = y < rect.height / 2.0 ? CGFloat(2 * y) / rect.height : 2.0 * CGFloat(rect.height - y) / rect.height
            saturation = CGFloat(powf(Float(saturation), y < rect.height / 2.0 ? saturationExponentTop : saturationExponentBottom))
            let brightness = y < rect.height / 2.0 ? CGFloat(1.0) : 2.0 * CGFloat(rect.height - y) / rect.height
            
            for x in stride(from: (0 as CGFloat), to: rect.width, by: elementSize) {
                let hue = x / rect.width
                let color = UIColor(hue: hue, saturation: saturation, brightness: brightness, alpha: 1.0)
                context!.setFillColor(color.cgColor)
                context!.fill(CGRect(x:x, y:y, width:elementSize,height:elementSize))
            }
        }
    }
    
    func getColorAtPoint(point:CGPoint) -> UIColor {
        let roundedPoint = CGPoint(x:elementSize * CGFloat(Int(point.x / elementSize)),
                                   y:elementSize * CGFloat(Int(point.y / elementSize)))
        var saturation = roundedPoint.y < self.bounds.height / 2.0 ? CGFloat(2 * roundedPoint.y) / self.bounds.height
            : 2.0 * CGFloat(self.bounds.height - roundedPoint.y) / self.bounds.height
        saturation = CGFloat(powf(Float(saturation), roundedPoint.y < self.bounds.height / 2.0 ? saturationExponentTop : saturationExponentBottom))
        let brightness = roundedPoint.y < self.bounds.height / 2.0 ? CGFloat(1.0) : 2.0 * CGFloat(self.bounds.height - roundedPoint.y) / self.bounds.height
        let hue = roundedPoint.x / self.bounds.width
        return UIColor(hue: hue, saturation: saturation, brightness: brightness, alpha: 1.0)
    }
    
    func getPointForColor(color:UIColor) -> CGPoint {
        var hue:CGFloat=0;
        var saturation:CGFloat=0;
        var brightness:CGFloat=0;
        color.getHue(&hue, saturation: &saturation, brightness: &brightness, alpha: nil);
        
        var yPos:CGFloat = 0
        let halfHeight = (self.bounds.height / 2)
        
        if (brightness >= 0.99) {
            let percentageY = powf(Float(saturation), 1.0 / saturationExponentTop)
            yPos = CGFloat(percentageY) * halfHeight
        } else {
            //use brightness to get Y
            yPos = halfHeight + halfHeight * (1.0 - brightness)
        }
        
        let xPos = hue * self.bounds.width
        
        return CGPoint(x: xPos, y: yPos)
    }
    
    func touchedColor(gestureRecognizer: UILongPressGestureRecognizer){
        let point = gestureRecognizer.location(in: self)
        let color = getColorAtPoint(point: point)
        
        self.delegate?.HSBColorColorPickerTouched(sender: self, color: color, point: point, state:gestureRecognizer.state)
    }
}

