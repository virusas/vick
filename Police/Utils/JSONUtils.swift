//
//  JSONUtils.swift
//  Police
//
//  Created by Crossover on 6/11/18.
//  Copyright © 2018 Crossover. All rights reserved.
//

import Foundation

class JSONUtils {
    private static let TAG = "JSONUtils"
    
    static func readJSONObject(_ filePath: String, _ fileType: String) -> [String: Any]? {
        do {
            if let url = Bundle.main.url(forResource: filePath, withExtension: fileType) {
                let data = try Data(contentsOf: url)
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } else {
                return nil
            }
        } catch {
            NSLog("[\(TAG)] Error parsing file \(filePath).\(fileType).")
            print(error.localizedDescription)
            return nil
        }
    }
    
    static func readJSONArray(_ filePath: String, _ fileType: String) -> [[String: Any]]? {
        do {
            if let url = Bundle.main.url(forResource: filePath, withExtension: fileType) {
                let data = try Data(contentsOf: url)
                return try JSONSerialization.jsonObject(with: data, options: []) as? [[String: Any]]
            } else {
                return nil
            }
        } catch {
            NSLog("[\(TAG)] Error parsing file \(filePath).\(fileType).")
            print(error.localizedDescription)
            return nil
        }
    }
}
