//
//  GameNavController.swift
//  Police
//
//  Created by Aluminum on 8/15/17.
//  Copyright © 2017 Crossover. All rights reserved.
//

import UIKit

// This class is used to force the child view controllers to run in portrait
class GameNavController: UINavigationController {
    override var shouldAutorotate: Bool {
        return false
    }
    
    override var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
        return .portrait
    }
}
