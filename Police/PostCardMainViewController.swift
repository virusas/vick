//
//  PostCardMainViewController.swift
//  Police
//
//  Created by Crossover on 6/15/18.
//  Copyright © 2018 Crossover. All rights reserved.
//

import UIKit

class PostCardMainViewController: UIViewController {
    private static let storyboardName = "MessageCreation"
    
    var previewImage : UIImageView = UIImageView.init()
    var str : String = "";
    var intId : Int = 0;
    var header: String = "";
    var content: String = "";
    var footer: String = "";
    @IBOutlet var topconstraint :NSLayoutConstraint? = NSLayoutConstraint();
   
    
    class func newInstance() -> UIViewController? {
        let storyboard = UIStoryboard(name: storyboardName, bundle: nil)
        return storyboard.instantiateInitialViewController()
    }

    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet weak var txtContent: UITextView!
    @IBOutlet weak var lblFooter: UILabel!
    
    var btnCancel: UIBarButtonItem!
    var btnSave: UIBarButtonItem!
    var image5: UIImage!
    //@IBOutlet weak var previewImage: UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.showiv()
        topconstraint?.constant = 252;
        // Do any additional setup after loading the view
        btnCancel = UIBarButtonItem(title: "action_back".localized(), style: .plain, target: self, action: #selector(onBarButtonClicked(_:)))
        btnSave = UIBarButtonItem(title: "action_save".localized(), style: .plain, target: self, action: #selector(onBarButtonClicked(_:)))
        navigationItem.leftBarButtonItem = btnCancel
        navigationItem.rightBarButtonItem = btnSave
        
        NotificationCenter.default.addObserver(self, selector: #selector(showKB(notification:)), name: .showKB, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(hideKB(notification:)), name: .hideKB, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(changeimg(notification:)), name: .image, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(changeid(notification:)), name: .id, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(changeheader(notification:)), name: .header, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(changecontent(notification:)), name: .content, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(changefooter(notification:)), name: .footer, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(changeHeaderColor(notification:)), name: .headerColor, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(changeContentColor(notification:)), name: .contentColor, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(changeFooterColor(notification:)), name: .footerColor, object: nil)
        
        if let header  : String = UserDefaults.standard.string(forKey: "post-card-header") {
            lblHeader.text = header as! String
        }
        if let content  : String  = UserDefaults.standard.string(forKey: "post-card-content") {
            txtContent.text = content as! String
        }
        if let footer  : String  = UserDefaults.standard.string(forKey: "post-card-footer") {
            lblFooter.text = footer as! String
        }
        if let headercolor : UIColor = UserDefaults.standard.color(forKey: "post-card-header-color")  {
            lblHeader.textColor = headercolor
        }
        if let contentcolor : UIColor  = UserDefaults.standard.color(forKey: "post-card-content-color")  {
            txtContent.textColor = contentcolor
        }
        if let footercolor  : UIColor = UserDefaults.standard.color(forKey: "post-card-footer-color") {
            lblFooter.textColor = footercolor
        }
    }
    // MARK: - Actions
    @objc func changeheader(notification: Notification) {
        if let str : String = notification.object as! String{
            lblHeader.text = str
        }
    }
    // MARK: - Actions
    @objc func changecontent(notification: Notification) {
        if let str : String = notification.object as! String{
            txtContent.text = str
        }
    }
    // MARK: - Actions
    @objc func changefooter(notification: Notification) {
        if let str : String = notification.object as! String{
            lblFooter.text = str
        }
    }
    // MARK: - Actions
    @objc func changeHeaderColor(notification: Notification) {
        if let color : UIColor = notification.object as! UIColor{
            lblHeader.textColor = color
        }
    }
    // MARK: - Actions
    @objc func changeContentColor(notification: Notification) {
        if let color : UIColor = notification.object as! UIColor{
            txtContent.textColor = color
        }
    }
    // MARK: - Actions
    @objc func changeFooterColor(notification: Notification) {
        if let color : UIColor = notification.object as! UIColor{
            lblFooter.textColor = color
        }
    }
    
    // MARK: - Actions
    @objc func showKB(notification: Notification) {
        topconstraint?.constant = 0;
    }
    
    // MARK: - Actions
    @objc func hideKB(notification: Notification) {
        topconstraint?.constant = 252;
    }
    
    // MARK: - Actions
    @objc func changeimg(notification: Notification) {
        if let img : UIImage = notification.object as! UIImage{
            self.previewImage.image = img
        }
    }
    // MARK: - Actions
    @objc func changeid(notification: Notification) {
        if let id : Int = notification.object as! Int{
            self.intId = id
        }
    }
    
    @objc func onBarButtonClicked(_ sender: UIBarButtonItem) {
        switch sender {
        case btnCancel:
            self.dismiss(animated: true, completion: nil)
        case btnSave:
            
            let defaults = UserDefaults.standard
            defaults.set(lblHeader.text, forKey: "post-card-header")
            defaults.set(txtContent.text, forKey: "post-card-content")
            defaults.set(lblFooter.text, forKey: "post-card-footer")
            defaults.set(lblHeader.textColor, forKey: "post-card-header-color")
            defaults.set(txtContent.textColor, forKey: "post-card-content-color")
            defaults.set(lblFooter.textColor, forKey: "post-card-footer-color")
            defaults.set(intId, forKey: "post-card-id")
            
            let alert = UIAlertController(title: "", message: "action_saved".localized(), preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "action_done".localized(), style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        default:
            return
        }
    }
    func showiv() -> Void {
        previewImage.frame = CGRect.init()
        previewImage.contentMode = UIViewContentMode.scaleAspectFit
        self.view.addSubview(previewImage)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
extension Notification.Name {
    static let image = Notification.Name("image")
    static let id = Notification.Name("id")
    static let header = Notification.Name("header")
    static let content = Notification.Name("content")
    static let footer = Notification.Name("footer")
    static let headerColor = Notification.Name("headerColor")
    static let contentColor = Notification.Name("contentColor")
    static let footerColor = Notification.Name("footerColor")
    static let showKB = Notification.Name("showKB")
    static let hideKB = Notification.Name("hideKB")
}

extension UserDefaults {
    
    func color(forKey key: String) -> UIColor? {
        var color: UIColor?
        if let colorData = data(forKey: key) {
            color = NSKeyedUnarchiver.unarchiveObject(with: colorData) as? UIColor
        }
        return color
    }
    
    func set(_ value: UIColor?, forKey key: String) {
        var colorData: Data?
        if let color = value {
            colorData = NSKeyedArchiver.archivedData(withRootObject: color)
        }
        set(colorData, forKey: key)
    }
    
}
