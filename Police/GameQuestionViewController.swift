//
//  GameQuestionViewController.swift
//  Police
//
//  Created by Aluminum on 8/16/17.
//  Copyright © 2017 Crossover. All rights reserved.
//

import UIKit
import Localize_Swift

class GameQuestionViewController: UIViewController {
    private let TAG: String = "GameQuestionViewController"
    @IBOutlet var questionLabel: UILabel!
    @IBOutlet var answerContainerView: UIView!
    @IBOutlet var answerContainerHeightConstraint: NSLayoutConstraint!
    @IBOutlet var submitButton: UIButton!
    
    var question: QuestionInfo!
    var questionWeight : Int!
    var answerTableVC: GameQuestionTableViewController!
    let manager: BooksManager!
    
    var selectedAnswer, finalScore: Int
    
    class func gameQuestionViewControllerForStep(_ questionInfo: QuestionInfo) -> GameQuestionViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "GameQuestionViewController") as! GameQuestionViewController
        viewController.question = questionInfo
        viewController.questionWeight = 100
        viewController.answerTableVC = GameQuestionTableViewController.gameQuestionTableForVC(questionInfo.mAnswers)
        return viewController
    }
    
    required init?(coder aDecoder: NSCoder) {
        selectedAnswer = -1
        finalScore = 0
        manager = BooksManager.init()
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        questionLabel.text = question.mQuestion
        
        finalScore = questionWeight
        
        answerTableVC.willMove(toParentViewController: self)
        answerTableVC.view.frame = answerContainerView.bounds
        answerTableVC.view.layer.masksToBounds = true
        answerTableVC.view.layer.cornerRadius = CGFloat(8)
        answerContainerView.addSubview(answerTableVC.view)
        addChildViewController(answerTableVC)
        answerTableVC.didMove(toParentViewController: self)
        
        // Locale
        NotificationCenter.default.addObserver(self, selector: #selector(onLanguageChange), name: NSNotification.Name(LCLLanguageChangeNotification), object: nil)
        onLanguageChange()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        answerContainerHeightConstraint.constant = answerTableVC.calculateTableHeight()
    }
    
    @objc func onLanguageChange() {
        submitButton.setTitle("action_submit".localized(), for: .normal)
    }
    
    @IBAction func onSubmitPressed(_ sender: UIButton) {
        if !question.mAnswers[selectedAnswer].mCorrect {
            answerTableVC.canSelect[selectedAnswer] = false
            answerTableVC.tableView.reloadData()
            
            finalScore -= Int(Float(questionWeight) / Float(question.mAnswers.count))
            
            let alert = UIAlertController(title: nil, message: "message_incorrect_answer".localized(), preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "action_tips".localized(), style: .default) {
                UIAlertAction in
                self.actionTips()
            })
            alert.addAction(UIAlertAction(title: "action_dismiss".localized(), style: .cancel))
            present(alert, animated: true, completion: nil)
        } else {
            let viewController = parent as? GameViewController
            if viewController == nil {
                NSLog("[\(TAG)] Unable to cast parent view controller to GameViewController")
                return
            }
            
            viewController!.addScore(finalScore)
            viewController!.nextStep()
        }
    }
    
    func actionTips() {
        NSLog("[\(self.TAG)] tips ing")
        //guard
            let bookName = question.mHint?.mBookName
            let chapter = question.mHint?.mChapter
            let chapterIndex = question.mHint?.mChapterIndex
            //let image = question.mHint?.mImage
        //else {
        //        return
        //}
        NSLog("[\(self.TAG)] bookName " + String(bookName!))
        NSLog("[\(self.TAG)] chapter " + String(chapter!))
        NSLog("[\(self.TAG)] chapterIndex " + String(chapterIndex!))
        let chapterInfo = manager.getChapterIndex(bookName!, chapter!, chapterIndex!)
        let viewController = BookReaderViewController.bookReaderViewControllerForChapterIndex(chapterInfo!)
        viewController.modalPresentationStyle = .popover
        
        if let navigator = navigationController {
            navigator.pushViewController(viewController, animated: true)
        }
    }
}
