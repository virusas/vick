//
//  DrawerMenuTableViewController.swift
//  Police
//
//  Created by Aluminum on 7/27/17.
//  Copyright © 2017 Crossover. All rights reserved.
//

import UIKit
import Localize_Swift

class DrawerTableViewController: UITableViewController {
    
    private let TAG: String = "DrawerMenuTableViewController"
    
    static let rowNames = [
        // Remember to mark these to Localizable.strings
        ["action_main_menu"],
        ["action_books",
         "action_pressure_test",
         "action_games",
         "action_anti_label",
         "action_resources",
         "action_post_card",
         "action_bookmarks"],
        ["action_appendix"]
    ]
    static let rowIcons = [
        [#imageLiteral(resourceName: "ic_menu_home")],
        [#imageLiteral(resourceName: "ic_menu_books"),
         #imageLiteral(resourceName: "ic_menu_games"),
         #imageLiteral(resourceName: "ic_menu_games"),
         #imageLiteral(resourceName: "ic_menu_books"),
         #imageLiteral(resourceName: "ic_menu_books"),
         #imageLiteral(resourceName: "ic_menu_gallery"),
         #imageLiteral(resourceName: "ic_menu_bookmarks")],
        [#imageLiteral(resourceName: "ic_menu_appendix")]
    ]
    
    private var selectedRow: Int! = -1
    private var selectedSection: Int! = -1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Register the user info table view cell
        //self.tableView.register(UINib(nibName: "UserInfoTableViewCell", bundle: nil), forCellReuseIdentifier: "User Info Cell")
        self.tableView.register(UITableViewCell(style: .default, reuseIdentifier: "Cell").classForCoder, forCellReuseIdentifier: "Cell")
        
        // Select default
        self.tableView.selectRow(at: IndexPath(row: 0, section: 0), animated: false, scrollPosition: .none)
        
        // Locale
        NotificationCenter.default.addObserver(self, selector: #selector(onLanguageChange), name: NSNotification.Name(LCLLanguageChangeNotification), object: nil)
    }
    
    @objc func onLanguageChange() {
        let previous = tableView.indexPathForSelectedRow
        tableView.reloadData()
        if previous != nil {
            changeFragment(indexPath: previous!, force: true)
        }
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return DrawerTableViewController.rowNames.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return DrawerTableViewController.rowNames[section].count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = "Cell"
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier)
        
        // Configure the cell...
        cell!.textLabel?.text = DrawerTableViewController.rowNames[indexPath.section][indexPath.row].localized()
        cell!.imageView?.image = DrawerTableViewController.rowIcons[indexPath.section][indexPath.row]
        return cell!
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        changeFragment(indexPath: indexPath)
    }
    
    override func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        // You can't see me, my time is now
        return "\0"
    }
    
    override func tableView(_ tableView: UITableView, willDisplayFooterView view: UIView, forSection section: Int) {
        // Footer is converted to 1px to act as a group divider
        view.tintColor = UIColor.lightGray
    }
    
    func changeFragment(indexPath: IndexPath, force: Bool = false) {
        let drawer = navigationController?.parent as! KYDrawerController
        if !force && selectedRow == indexPath.row && selectedSection == indexPath.section {
            // Close the drawer
            drawer.setDrawerState(.closed, animated: true)
            return
        }
        
        let title = DrawerTableViewController.rowNames[indexPath.section][indexPath.row]
        let storyboard = UIStoryboard(name: BookTableViewController.storyboardName, bundle: nil)
        var viewController: UIViewController?
        
        // Set up the different view controllers based on sender to show.
        switch indexPath.section {
//        case 0:
//            // User profile
//            break
        case 0:
            switch indexPath.row {
            // Main Menu
            case 0:
                viewController = storyboard.instantiateViewController(withIdentifier: "MainMenuViewController") as! MainMenuViewController
            default:
                break
            }
        case 1:
            switch indexPath.row {
            // eBook
            case 0:
                viewController = BookTableViewController.bookTableViewControllerForBook("education")
            // Pressure Test
            case 1:
                viewController = storyboard.instantiateViewController(withIdentifier: "PressureTestViewController")
            // Games
            case 2:
                viewController = storyboard.instantiateViewController(withIdentifier: "GameMainMenuViewController") as! GameMainMenuViewController
            // Anti-Label
            case 3:
                viewController = BookTableViewController.bookTableViewControllerForBook("anti_label")
            // Resources
            case 4:
                viewController = BookTableViewController.bookTableViewControllerForBook("resource")
            // Message Creation
            case 5:
                
                viewController = storyboard.instantiateViewController(withIdentifier: "PostCardViewController") as! PostCardViewController
            // Bookmarks
            case 6:
                viewController = storyboard.instantiateViewController(withIdentifier: "BookmarkTableViewController") as! BookmarkTableViewController
            default:
                break
            }
        case 2:
            switch indexPath.row {
            // Appendix
            case 0:
                viewController = BookTableViewController.bookTableViewControllerForBook("appendix")
            default:
                break
            }
        default:
            break
        }
        
        if viewController != nil {
            if let mainVC = drawer.mainViewController as? UINavigationController {
                viewController!.title = title.localized()
                mainVC.setViewControllers([viewController!], animated: true)
            }
            
            // Select row
            tableView.selectRow(at: IndexPath(row: indexPath.row, section: indexPath.section), animated: false, scrollPosition: .none)
            
            // Update the record
            selectedRow = indexPath.row
            selectedSection = indexPath.section
            
            // Close the drawer
            drawer.setDrawerState(.closed, animated: true)
        } else {
            NSLog("[\(TAG)] No view controller defined for selection \(indexPath.section)-\(indexPath.row)")
        }
    }
}
