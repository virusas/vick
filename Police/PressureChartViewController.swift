//
//  PressureChartViewController.swift
//  Police
//
//  Created by Crossover on 12/6/2018.
//  Copyright © 2018年 Crossover. All rights reserved.
//

import UIKit

class PressureChartViewController: BaseViewController {
    @IBOutlet weak var backButton: UIButton!
    //@IBOutlet weak var barChart: BasicBarChart!
    @IBOutlet var imgView: UIImageView!
    @IBOutlet weak var UITitle: UILabel!
    @IBOutlet weak var UIResult1: UILabel!
    @IBOutlet weak var UIResult2: UILabel!
    @IBOutlet weak var UIResult3: UITextView!
    
    var finalScore = Int.init()
    var value :[Int] = []
    var date :[String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        backButton.setTitle("action_back".localized(), for: .normal)
    }
    
    @IBAction func backPress(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        //let dataEntries = generateDataEntries()
        //barChart.dataEntries = dataEntries
        
        let defaults = UserDefaults.standard
        let finalvalue : Float = defaults.float(forKey: "presstest-finalScore")
        
        let height: Float = finalvalue / 93;
        
        //case low
        if(height>0.7){
            //case high
            DispatchQueue.main.async {
                self.imgView.image = UIImage(named: "gauge03")
            }
            UITitle.text = "pressure_result_title".localized();
            UIResult1.text = "pressure_result".localized();
            UIResult2.text = "pressure_result_high_highlight".localized();
            UIResult3.text = "pressure_result_high_end".localized() + "pressure_result_help".localized();
            
        }else if(height>0.3){
            //case middle
            DispatchQueue.main.async {
                self.imgView.image = UIImage(named: "gauge02")
            }
            UITitle.text = "pressure_result_title".localized();
            UIResult1.text = "pressure_result".localized();
            UIResult2.text = "pressure_result_middle_highlight".localized();
            UIResult3.text = "pressure_result_middle_end".localized() + "pressure_result_help".localized();
        }else{
            
            DispatchQueue.main.async {
                self.imgView.image = UIImage(named: "gauge01")
            }
            UITitle.text = "pressure_result_title".localized();
            UIResult1.text = "pressure_result".localized();
            UIResult2.text = "pressure_result_low_highlight".localized();
            UIResult3.text = "pressure_result_low_end".localized() + "pressure_result_help".localized();
        }
        
    }
    
    func generateDataEntries() -> [BarEntry] {
        let colors = [#colorLiteral(red: 0.4666666687, green: 0.7647058964, blue: 0.2666666806, alpha: 1), #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1), #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1), #colorLiteral(red: 0.9607843161, green: 0.7058823705, blue: 0.200000003, alpha: 1), #colorLiteral(red: 0.9372549057, green: 0.3490196168, blue: 0.1921568662, alpha: 1), #colorLiteral(red: 0.8078431487, green: 0.02745098062, blue: 0.3333333433, alpha: 1), #colorLiteral(red: 0.3647058904, green: 0.06666667014, blue: 0.9686274529, alpha: 1)]
        var result: [BarEntry] = []
        
        //}
        return result
    }
}
