//
//  BookReaderViewController.swift
//  Police
//
//  Created by Aluminum on 7/26/17.
//  Copyright © 2017 Crossover. All rights reserved.
//

import UIKit
import WebKit

class BookReaderViewController: UIViewController, WKNavigationDelegate, UIWebViewDelegate {

    // MARK: - Types
    
    private let TAG: String = "BookReaderViewController"
    
    // Constants for Storyboard/ViewControllers.
    static let storyboardName = "Main"
    static let viewControllerIdentifier = "BookReaderViewController"
    
    // MARK: - Properties
    @IBOutlet var bookmarkButton: UIBarButtonItem!
    
    // Switch WKWebView to UIWebView
    // var webView: WKWebView!
    var webView: UIWebView!
    var loadIndicator: UIActivityIndicatorView!
    
    var bookName: String!
    var chapter: Int!
    var chapterIndex: Int!
    //var image:String!
    var indexInfo: ChapterIndexInfo!
    
    // MARK: - Initialization
    
    class func bookReaderViewControllerForChapterIndex(_ indexInfo: ChapterIndexInfo) -> BookReaderViewController {
        let storyboard = UIStoryboard(name: BookReaderViewController.storyboardName, bundle: nil)
        
        let viewController = storyboard.instantiateViewController(withIdentifier: BookReaderViewController.viewControllerIdentifier) as! BookReaderViewController
        
        // Some of the values - 1 because display number starts from 1
        viewController.bookName = indexInfo.mOwningChapter.mOwningBook.mAssetName
        viewController.chapter = indexInfo.mOwningChapter.mNum - 1
        viewController.chapterIndex = indexInfo.mNum - 1
        viewController.indexInfo = indexInfo
        
        return viewController
    }
    
    // MARK: - View Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        title = indexInfo.mTitle
       
       /*
        let filePath1 = "ebooks/\(indexInfo.mImage)/\(indexInfo.mFilePath)"
        if let resourceURL = Bundle.main.url(forResource: filePath1, withExtension: "jpg"){
            do{
                let urlData = try Data(contentsOf: resourceURL as URL)
                webView.load(urlData, mimeType: "text/jpg", textEncodingName: "UTF-8", baseURL: resourceURL)
            }catch{
                NSLog("[\(TAG)] Unable to load data: \(error)")
            }
        }else {
            displayErrorMessage(message: "error_missing_page".localized())
        }
 */
        // Check if the resource exist first, so we can run faster on older devices
        let filePath = "ebooks/\(indexInfo.mOwningChapter.mOwningBook.mAssetName)/\(indexInfo.mFilePath)"
        if let resourceURL = Bundle.main.url(forResource: filePath, withExtension: "html") {
//            // Setup WKWebView
//            webView = WKWebView(frame: self.view.bounds)
//            webView.navigationDelegate = self
//            self.view.addSubview(webView)
            
            // Setup UIWebView
            webView = UIWebView(frame: self.view.bounds)
            // try to adjust the web view text size but not working
            let savedSize = UserDefaults().integer(forKey: "pref_text_size")
            webView.stringByEvaluatingJavaScript(from: "document.getElementsByTagName('body')[0].style.webkitTextSizeAdjust= '\(savedSize)%%'")
            webView.delegate = self
            self.view.addSubview(webView)
            
            // Setup UIActivityIndicatorView
            var frame = CGRect(origin: webView.center, size: CGSize(width: 50, height: 50))
            frame.origin.x -= 25
            loadIndicator = UIActivityIndicatorView(frame: frame)
            loadIndicator.activityIndicatorViewStyle = .whiteLarge
            loadIndicator.color = UIColor.black
            loadIndicator.hidesWhenStopped = true
            webView.addSubview(loadIndicator)
            
            // Load page
            //webView.loadRequest(URLRequest(url: resourceURL))
            do{
                let urlData = try Data(contentsOf: resourceURL as URL)
                webView.load(urlData, mimeType: "text/html", textEncodingName: "UTF-8", baseURL: resourceURL)
                
                webView.stringByEvaluatingJavaScript(from: "document.getElementsByTagName('body')[0].style.webkitTextSizeAdjust= '\(savedSize)%%'")
                
                webView.delegate = self
                
            }catch{
                NSLog("[\(TAG)] Unable to load data: \(error)")
            }
        } else {
            displayErrorMessage(message: "error_missing_page".localized())
        }
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        webView.scalesPageToFit = true
        
        //let savedSize = UserDefaults().integer(forKey: "pref_text_size")
        //let scale = Float(savedSize/300)
        let str = "var meta = document.createElement('meta');"
        let str1 = "meta.setAttribute( 'name', 'viewport' ); "
        //let str1 = "meta.setAttribute( 'UTF-8', 'charset' ); "

        let str2 = "meta.setAttribute( 'content', 'width = device-width, initial-scale = 1.0, user-scalable = yes' ); "
        let str3 = "document.getElementsByTagName('head')[0].appendChild(meta)"
        
        let savedSize = UserDefaults().integer(forKey: "pref_text_size")
        webView.stringByEvaluatingJavaScript(from: "document.getElementsByTagName('body')[0].style.webkitTextSizeAdjust= '\(savedSize)%%'")
        
        //webView.stringByEvaluatingJavaScript(from: "\(str)\(str1)\(str2)\(str3)")
        //webView.reload()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        updateBookmarkState(bookmarkButton)
    }
    
    deinit {
        loadIndicator?.removeFromSuperview()
        webView?.removeFromSuperview()
    }
    
    func displayErrorMessage(message: String) {
        let error = UIAlertController(
            title: "message_load_failed".localized(),
            message: message,
            preferredStyle: .alert)
        error.addAction(UIAlertAction(
            title: "action_dismiss".localized(),
            style: UIAlertActionStyle.cancel,
            handler: { action in
                //self.navigationController?.popViewController(animated: true)
                return
            }
            )
        )
        self.present(error, animated: true, completion: nil)
    }
    
    // MARK: - Database functions
    
    @IBAction func toggleBookmark(_ sender: UIBarButtonItem) {
        if bookmarkExist() {
            removeBookmark()
        } else {
            createBookmark()
        }
        
        updateBookmarkState(sender)
    }
    
    func bookmarkExist() -> Bool {
        var result = false
        var dbHelper = PoliceDBHelper()
        
        // Make query
        let query = "SELECT COUNT(*) FROM \(PoliceContract.BookmarkEntry.TABLE_NAME) WHERE \(PoliceContract.BookmarkEntry.COLUMN_NAME_BOOK) = '\(bookName!)' AND \(PoliceContract.BookmarkEntry.COLUMN_NAME_CHAPTER) = \(chapter!) AND \(PoliceContract.BookmarkEntry.COLUMN_NAME_CHAPTER_INDEX) = \(chapterIndex!)"
        let statement = dbHelper?.query(query)
        
        if sqlite3_step(statement) == SQLITE_ROW {
            result = Int(sqlite3_column_int(statement, 0)) == 1
        }
        
        sqlite3_finalize(statement)
        dbHelper = nil
        return result
    }
    
    func createBookmark() {
        var dbHelper = PoliceDBHelper()
        
        let statement = "INSERT INTO \(PoliceContract.BookmarkEntry.TABLE_NAME) (\(PoliceContract.BookmarkEntry.COLUMN_NAME_BOOK),\(PoliceContract.BookmarkEntry.COLUMN_NAME_CHAPTER),\(PoliceContract.BookmarkEntry.COLUMN_NAME_CHAPTER_INDEX)) VALUES ('\(bookName!)',\(chapter!),\(chapterIndex!))"
        
        dbHelper?.execute(statement)
        dbHelper = nil
    }
    
    func removeBookmark() {
        var dbHelper = PoliceDBHelper()
        
        let statement = "DELETE FROM \(PoliceContract.BookmarkEntry.TABLE_NAME) WHERE \(PoliceContract.BookmarkEntry.COLUMN_NAME_BOOK) = '\(bookName!)' AND \(PoliceContract.BookmarkEntry.COLUMN_NAME_CHAPTER) = \(chapter!) AND \(PoliceContract.BookmarkEntry.COLUMN_NAME_CHAPTER_INDEX) = \(chapterIndex!))"
        
        dbHelper?.execute(statement)
        dbHelper = nil
    }
    
    func updateBookmarkState(_ item: UIBarButtonItem) {
        let bookmarked = bookmarkExist()
        
        // Set icon
        let drawable = bookmarked ? #imageLiteral(resourceName: "ic_option_remove_bookmark") : #imageLiteral(resourceName: "ic_option_create_bookmark")
        item.image = drawable
    }
    
    // MARK: - WKNavigationDelegate
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        loadIndicator.startAnimating()
    }
    
    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        loadIndicator.stopAnimating()
        displayErrorMessage(message: error.localizedDescription)
    }
    
    func webView(_ webView: WKWebView, didCommit navigation: WKNavigation!) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let textSize = appDelegate.getTextSize()
        webView.evaluateJavaScript("document.getElementsByTagName('body')[0].style.webkitTextSizeAdjust='\(textSize)%'", completionHandler: nil)
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        loadIndicator.stopAnimating()
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        if navigationAction.navigationType == .linkActivated {
            let newURL = navigationAction.request.url
            if UIApplication.shared.canOpenURL(newURL!) {
                let alert = UIAlertController(title: "dialog_open_hyperlink".localized(), message: navigationAction.request.url?.absoluteString, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "action_cancel".localized(), style: .cancel, handler: { (action: UIAlertAction!) in
                    decisionHandler(.cancel)
                }))
                alert.addAction(UIAlertAction(title: "action_ok".localized(), style: .default, handler: { (action: UIAlertAction!) in
                    decisionHandler(.cancel)
                    UIApplication.shared.openURL(newURL!)
                }))
                present(alert, animated: true, completion: nil)
            } else {
                NSLog("[\(TAG)] linkActivated contains un-openable url '\(newURL?.absoluteString ?? "empty")'")
                decisionHandler(.cancel)
            }
        } else {
            decisionHandler(.allow)
        }
    }
}
