//
//  AppDelegate.swift
//  Police
//
//  Created by Aluminum on 7/20/17.
//  Copyright © 2017 Crossover. All rights reserved.
//

import UIKit
import Localize_Swift
import Floaty

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate /*,FloatyDelegate*/ {
    var window: UIWindow?
    var booksMgr: BooksManager!
    var gamesMgr: GamesManager!
    var pressureMgr: PressureManager!
    var floaty = Floaty()

    var debugInstallId: String! = nil

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.

        // We got bigger problems if these are missing...
        booksMgr = BooksManager()
        gamesMgr = GamesManager()
        pressureMgr = PressureManager()
        floaty = Floaty()
        
        // Set default values for settings
        UserDefaults.standard.register(defaults: ["pref_app_language": "zh-Hant", "pref_text_size" : 100])
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    enum ValidationResult {
        case success
        case wrongPassword
        case serverUnreachable
    }
    
    
    func validatePassword(password: String) -> ValidationResult {
        //if DEBUG
        if password == "psg18Y04m30d" {
             return ValidationResult.success
        }
            // For debug builds, always return success
        //else
            // TODO: Validation
            if password != "psg18Y04m30d" {
                return ValidationResult.wrongPassword
            } else {
                return ValidationResult.serverUnreachable
            }
        //endif
    }
    
    func haveInstallationId() -> Bool {
        return UserDefaults.standard.object(forKey: "pref_installation_id") != nil
    }
    
    func setInstallationId() {
        let userId = generateRandomString(length: 12)
        UserDefaults.standard.set(userId, forKey: "pref_installation_id")
        UserDefaults.standard.set(0, forKey: "pref_user_level")
    }
    
    func getInstallationId() -> String? {
        return UserDefaults.standard.value(forKey: "pref_installation_id") as? String
    }
    
    // Return the string of current locale based on our preference
    func getLanguageText() -> String {
        let result = UserDefaults.standard.value(forKey: "pref_app_language") as? String
        return result ?? "zh-tw"
    }
    
    func getTextSize() -> Int {
        let result = UserDefaults.standard.value(forKey: "pref_text_size") as? Int
        return result ?? 100
    }
    
    func generateRandomString(length: Int) -> String? {
        if length <= 0 {
            return nil
        }
        
        let availableChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"
        
        var result = ""
        for _ in 0 ..< length {
            let index = Array(availableChars)[Int(arc4random_uniform(UInt32(availableChars.count)))]
            result.append(index)
        }
        
        return result
    }
}
