//
//  PressureTestViewController.swift
//  Police
//
//  Created by Crossover on 29/5/2018.
//  Copyright © 2018年 Crossover. All rights reserved.
//

import UIKit

class PressureTestStartViewController: BaseViewController {
    
    @IBOutlet weak var pressureTitleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var startButton: UIButton!
    @IBOutlet weak var chartButton: UIButton!
    
    // MARK: - Types
    private let TAG: String = "PressureTestStartViewController"
    
    // Constants for Storyboard/ViewControllers.
    static let storyboardName = "Main"
    static let viewControllerIdentifier = "PressureTestStartViewController"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        pressureTitleLabel.text = "pressure_title".localized()
        descriptionLabel.text = "pressure_description".localized()
        startButton.setTitle("action_start_pressure".localized(), for: .normal)
        chartButton.setTitle("action_start_chart".localized(), for: .normal)
    }
    override func viewWillAppear(_ animated: Bool) {
        if UserDefaults.standard.value(forKey: "presstest-finalScore") != nil{
            chartButton.isHidden = false
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func startTest(_ sender: UIButton) {
        if let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PressureQuestionViewController") as? PressureQuestionViewController {
            if let navigator = navigationController {
                navigator.pushViewController(viewController, animated: true)
            }
        }
    }
    
    
    @IBAction func chartOpen(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: BookTableViewController.storyboardName, bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "PressureChartViewController") as! PressureChartViewController

        self.present(viewController, animated: true, completion: nil)
    }
}
