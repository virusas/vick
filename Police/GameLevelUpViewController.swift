//
//  GameLevelUpViewController.swift
//  Police
//
//  Created by Crossover on 19/6/2018.
//  Copyright © 2018年 Crossover. All rights reserved.
//

import UIKit

class GameLevelUpViewController: BaseViewController {
    var mLevel: Int!
    
    @IBOutlet weak var levelUpImg: UIImageView!
    @IBOutlet weak var levelUpText: UILabel!
    
    var stageInfo: StageInfo!
    
    class func newInstance() -> GameLevelUpViewController? {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "LevelUp") as? GameLevelUpViewController
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        let userDefaults = UserDefaults()
        
        // Set level
        mLevel = (userDefaults.object(forKey: "pref_user_level") as? Int ?? 0) + 1 
        userDefaults.set(mLevel, forKey: "pref_user_level")
        NSLog("[Done] mLevel = " + String(mLevel))
        // Set image
        //let userImg = UserDefaults().string(userImg,forKey: "userImg")
        let userImg = "pc_m"
        var image: String
        //if let userImg = UserDefaults().string(forKey: "userImg") {
            NSLog("[Done] userImg = " + userImg + " , mLevel = " + String(mLevel))
            switch mLevel {
            case 1:
                image = "\(userImg)_cert"
                levelUpText.text = "congrat1".localized()
            case 2:
                image = "\(userImg)_lanyard"
                levelUpText.text = "congrat2".localized()
            case 3:
                image = "\(userImg)_medal"
                levelUpText.text = "congrat3".localized()
            default:
                image = userImg
                levelUpText.text = "Error"
            }
       /*
     else {
            image = "pc_m"
            levelUpText.text = "Error"
        }
         */
        levelUpImg.image = UIImage(named: image)
    }

    @IBAction func NextButtonClicked(_ sender: UIButton) {
        if let controller = storyboard?.instantiateViewController(withIdentifier: "GameMainMenuViewController"){
            show(controller, sender: self)
        }
       //if let viewController = parent as? GameViewController
            //stageInfo.mLevelUp = true
          //self.dismiss(animated: true, completion: nil)
    }
  
}
