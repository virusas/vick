//
//  PressureQuestionViewController.swift
//  Police
//
//  Created by Crossover on 11/6/2018.
//  Copyright © 2018年 Crossover. All rights reserved.
//

import UIKit

class PressureQuestionTableViewCell: UITableViewCell {
    @IBOutlet var contentLabel: UILabel!
}

class PressureQuestionTableViewController: UITableViewController {
    static let rowHeight = 32
    
    private let TAG: String = "PressureQuestionTableViewController"
    
    var answers: [AnswerInfo]!
    var canSelect: [Bool]!
    
    class func PressureQuestionTableForVC(_ answers: [AnswerInfo]) -> PressureQuestionTableViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "PressureQuestionTableViewController") as! PressureQuestionTableViewController
        viewController.answers = answers
        viewController.canSelect = Array(repeating: true, count: answers.count)
        return viewController
    }
    
    override func viewDidLoad() {
        tableView.estimatedRowHeight = CGFloat(PressureQuestionTableViewController.rowHeight)
        tableView.rowHeight = UITableViewAutomaticDimension
    }
    
    func calculateTableHeight() -> CGFloat {
        var result = CGFloat(0)
        for i in 0 ..< tableView.numberOfRows(inSection: 0) {
            result += tableView.rectForRow(at: IndexPath(row: i, section: 0)).height
        }
        return result
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return answers.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! PressureQuestionTableViewCell
        
        // Configure the cell...
        cell.contentLabel?.text = answers[indexPath.row].mAnswer
        if !canSelect[indexPath.row] {
            cell.contentLabel?.textColor = UIColor.lightGray
        }
        cell.accessoryType = .none
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        return canSelect[indexPath.row]
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // Remove all accessory on each section
        for i in 0 ..< tableView.numberOfRows(inSection: indexPath.section) {
            tableView.cellForRow(at: IndexPath(row: i, section: indexPath.section))?.accessoryType = .none
        }
        
        // Deselect selected
        tableView.deselectRow(at: indexPath, animated: false)
        tableView.cellForRow(at: indexPath)?.accessoryType = .checkmark
        
        let viewController = parent as? PressureQuestionViewController
        if viewController == nil {
            NSLog("[\(TAG)] Unable to cast parent view controller to PressureQuestionTableViewCell")
            return
        }
        
        viewController!.selectedAnswer = indexPath.row
//        viewController!.submitButton.isEnabled = viewController!.selectedAnswer != -1
    }
}
