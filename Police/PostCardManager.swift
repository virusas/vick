//
//  PostCardManager.swift
//  Police
//
//  Created by Aluminum on 8/28/17.
//  Copyright © 2017 Crossover. All rights reserved.
//
import UIKit
import Foundation
import Localize_Swift


struct PostCardMessageGroup {
    let mType: Int
    var mMessages: [String]
}

extension PostCardMessageGroup {
    init?(json: [String: Any], locale: String) {
        guard let type = json["type"] as? Int,
            let messages = json["messages"] as? [[String: Any]]
            else {
                return nil
        }
        
        mType = type
        mMessages = [String]()
        for message in messages {
            if let messageLocalized = message[locale] as? String {
                mMessages.append(messageLocalized)
            }
        }
    }
}

class PostCardManager {
    static let instance = PostCardManager()

    private let TAG: String = "PostCardManager"
    
    private var mImages: [String]!
    private var mMessageGroups = [PostCardMessageGroup]()
   
    private init() {
        prepareImages()
        purgeCache()
    }
    
    func purgeCache() {
        prepareMessages()
    }
    
    func getImages() -> [String]? {
        return mImages != nil ? mImages : nil
    }
    
    func getMessageGroups() -> [PostCardMessageGroup]? {
        return mMessageGroups.count > 0 ? mMessageGroups : nil
    }
    
    func getMessageGroup(_ type: Int) -> PostCardMessageGroup? {
        for messageGroup in mMessageGroups {
            if messageGroup.mType == type {
                return messageGroup
            }
        }
        
        return nil
    }
    
    private func prepareImages() {
        if let resPath = Bundle.main.resourceURL {
            do {
                var postcardPath = resPath
                postcardPath.appendPathComponent("post_card/images/")
                mImages = try FileManager().contentsOfDirectory(atPath: postcardPath.path)
            } catch {
                NSLog("[\(TAG)] Resource path invalid!")
                print(error.localizedDescription)
            }
        }
    }
    
    private func prepareMessages() {
        // Get locale settting
        var locale = Localize.currentLanguage()
        if locale == "zh-Hant" {
            // We need to warp this to zh-tw to cope with assets
            locale = "zh-tw"
        }
        
        // Build message groups
        if let jsonArray = JSONUtils.readJSONArray("post_card/messages", "json") {
            // Purge data
            mMessageGroups.removeAll()
            
            for jsonData in jsonArray {
                if let messageGroup = PostCardMessageGroup(json: jsonData, locale: locale) {
                     mMessageGroups.append(messageGroup)
                }
            }
        }
    }
}
