//
//  PostCardViewController.swift
//  Police
//
//  Created by Crossover on 11/6/2018.
//  Copyright © 2018年 Crossover. All rights reserved.
//

import UIKit
import Localize_Swift

class PostCardViewController: BaseViewController{
    
    @IBOutlet weak var postCard_title: UILabel!
    @IBOutlet weak var postCard_Next: UILabel!
    @IBOutlet weak var startButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        postCard_title.text = "start_title".localized()
        postCard_Next.text = "start_text".localized()
        startButton.setTitle("start_button".localized(), for: .normal)
    }

    @IBAction func startButtonClicked(_ sender: Any) {
        if let vc = PostCardMainViewController.newInstance() {
            self.present(vc, animated: true, completion: nil)
        }
    }
    
}
