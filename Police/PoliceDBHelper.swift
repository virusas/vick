//
//  DatabaseManager.swift
//  Police
//
//  Created by Aluminum on 7/28/17.
//  Copyright © 2017 Crossover. All rights reserved.
//

import Foundation

// iOS - We don't have SQLiteOpenHelper, let alone SQLiteDatabase!
// All database actions will be done here.
class PoliceDBHelper {
    private final let TAG: String = "PoliceDBHelper"
    private static let DATABASE_VERSION: Int = 1
    private static let DATABASE_NAME: String = "local_data.db"
    
    private static let DISPLAY_VERBOSE: Bool = false
    private static let DISPLAY_COMMANDS: Bool = false
    
    private let SQL_CREATE_BOOKMARK: String =
        "CREATE TABLE " + PoliceContract.BookmarkEntry.TABLE_NAME + " (" +
            "_id INTEGER PRIMARY KEY AUTOINCREMENT," +
            PoliceContract.BookmarkEntry.COLUMN_NAME_BOOK + " TEXT," +
            PoliceContract.BookmarkEntry.COLUMN_NAME_CHAPTER + " INTEGER," +
            PoliceContract.BookmarkEntry.COLUMN_NAME_CHAPTER_INDEX + " INTEGER," +
            PoliceContract.BookmarkEntry.COLUMN_NAME_IMAGE + " TEXT)"
    private let SQL_DELETE_BOOKMARK: String =
        "DROP TABLE IF EXISTS " + PoliceContract.BookmarkEntry.TABLE_NAME
    
    private let SQL_CREATE_GAME_RECORD: String =
        "CREATE TABLE " + PoliceContract.GameRecordEntry.TABLE_NAME + " (" +
            PoliceContract.GameRecordEntry.COLUMN_NAME_CODENAME + " TEXT," +
            PoliceContract.GameRecordEntry.COLUMN_NAME_VERSION + " INTEGER," +
            PoliceContract.GameRecordEntry.COLUMN_NAME_COMPLETED + " INTEGER," +
            PoliceContract.GameRecordEntry.COLUMN_NAME_COMPLETION_DATE + " INTEGER," +
            "PRIMARY KEY (" + PoliceContract.GameRecordEntry.COLUMN_NAME_CODENAME + "," + PoliceContract.GameRecordEntry.COLUMN_NAME_VERSION + "))"
    private let SQL_DELETE_GAME_RECORD: String =
        "DROP TABLE IF EXISTS " + PoliceContract.GameRecordEntry.TABLE_NAME
    
    private let SQL_CREATE_PRESSURE_TEST: String =
        "CREATE TABLE " + PoliceContract.PressureTestEntry.TABLE_NAME + " (" +
            PoliceContract.PressureTestEntry.COLUMN_NAME_DATE + " DATETIME DEFAULT CURRENT_TIMESTAMP," +
            PoliceContract.PressureTestEntry.COLUMN_NAME_MARK + " INTEGER)"
    private let SQL_DELETE_PRESSURE_TEST: String =
        "DROP TABLE IF EXISTS " + PoliceContract.PressureTestEntry.TABLE_NAME
    
    private let SQL_CREATE_HEADER: String =
        "CREATE TABLE " + PoliceContract.DatabaseHeader.TABLE_NAME + " (" +
            PoliceContract.DatabaseHeader.COLUMN_NAME_VERSION + " INTEGER PRIMARY KEY)"
    private let SQL_INSERT_VERSION: String =
        "INSERT INTO " + PoliceContract.DatabaseHeader.TABLE_NAME + " VALUES(" +
            String(DATABASE_VERSION) + ")"
    private let SQL_GET_VERSION: String =
            "SELECT " + PoliceContract.DatabaseHeader.COLUMN_NAME_VERSION + " FROM " + PoliceContract.DatabaseHeader.TABLE_NAME + " LIMIT 1"
    private let SQL_DELETE_HEADER: String =
            "DROP TABLE IF EXISTS " + PoliceContract.DatabaseHeader.TABLE_NAME
    
    private var db: OpaquePointer? = nil
    
    // iOS - Open connecton to database
    init?() {
        let documentsDirectory = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
        let databasePath = documentsDirectory.appendingPathComponent(PoliceDBHelper.DATABASE_NAME).absoluteString
        
        if PoliceDBHelper.DISPLAY_VERBOSE {
            NSLog("[\(TAG)] Opening database \(databasePath)")
        }
        let result = sqlite3_open(databasePath, &db)
        if result != SQLITE_OK {
            NSLog("[\(TAG)] Unable to open database! Error code: \(result)")
            return nil
        }
    }
    
    // iOS - Close connection to database
    deinit {
        let result = sqlite3_close(db)
        if result != SQLITE_OK {
            NSLog("[\(TAG)] Unable to close database! Error code: \(result)")
        } else {
            if PoliceDBHelper.DISPLAY_VERBOSE {
                NSLog("[\(TAG)] Database successfully closed.")
            }
        }
    }
    
    // iOS - Check custom database version
    func versionCheck() {
        // Header exists
        var headerExists: Bool = false
        if let statement = query("SELECT COUNT(*) FROM sqlite_master WHERE type='table' AND name='\(PoliceContract.DatabaseHeader.TABLE_NAME)'") {
            if sqlite3_step(statement) == SQLITE_ROW {
                headerExists = Int(sqlite3_column_int(statement, 0)) == 1
            }
            sqlite3_finalize(statement)
        }
        
        if headerExists {
            // Version check
            var version: Int = -1
            if let statement = query(SQL_GET_VERSION) {
                if sqlite3_step(statement) == SQLITE_ROW {
                    version = Int(sqlite3_column_int(statement, 0))
                }
                sqlite3_finalize(statement)
            }
            
            if version != -1 {
                if version < PoliceDBHelper.DATABASE_VERSION {
                    onUpgrade(oldVersion: version, newVersion: PoliceDBHelper.DATABASE_VERSION)
                } else if version > PoliceDBHelper.DATABASE_VERSION {
                    onDowngrade(oldVersion: version, newVersion: PoliceDBHelper.DATABASE_VERSION)
                }
            } else {
                NSLog("[\(TAG)] Unable to retrive database version! Performing reset...")
                onUpgrade(oldVersion: PoliceDBHelper.DATABASE_VERSION, newVersion: PoliceDBHelper.DATABASE_VERSION)
            }
        } else {
            onCreate()
        }
    }
    
    // iOS - Execute command to database
    func execute(_ statement: String) {
        if PoliceDBHelper.DISPLAY_COMMANDS {
            NSLog("[\(TAG)] Executing command '\(statement)'.")
        }
        
        let cString = statement.cString(using: String.Encoding.utf8)
        var errMsg: UnsafeMutablePointer<Int8>?
        let result = sqlite3_exec(db, cString, nil, nil, &errMsg)
        if result != SQLITE_OK {
            NSLog("[\(TAG)] Execute error! Error code: \(result)")
            if errMsg != nil {
                print(String(cString: errMsg!))
            }
        }
    }
    
    // iOS - Get statement form database
    func query(_ statement: String) -> OpaquePointer? {
        if PoliceDBHelper.DISPLAY_COMMANDS {
            NSLog("[\(TAG)] Executing query '\(statement)'.")
        }
        
        var preparedStmt: OpaquePointer? = nil
        let cString = statement.cString(using: String.Encoding.utf8)
        var errMsg: UnsafePointer<Int8>?
        let result = sqlite3_prepare_v2(db, cString, -1, &preparedStmt, nil)
        if result != SQLITE_OK {
            NSLog("[\(TAG)] Prepare statment error! Error code: \(result)")
            errMsg = sqlite3_errmsg(db)
            if errMsg != nil {
                print(String(cString: errMsg!))
            }
            return nil
        }
        
        return preparedStmt
    }
    
    func onCreate() {
        execute(SQL_CREATE_HEADER)
        execute(SQL_INSERT_VERSION)
        execute(SQL_CREATE_BOOKMARK)
        execute(SQL_CREATE_GAME_RECORD)
        execute(SQL_CREATE_PRESSURE_TEST)
    }
    
    func onUpgrade(oldVersion: Int, newVersion: Int) {
        execute(SQL_DELETE_HEADER)
        execute(SQL_DELETE_BOOKMARK)
        execute(SQL_DELETE_GAME_RECORD)
        execute(SQL_DELETE_PRESSURE_TEST)
        onCreate()
    }
    
    func onDowngrade(oldVersion: Int, newVersion: Int) {
        onUpgrade(oldVersion: oldVersion, newVersion: newVersion)
    }
}
