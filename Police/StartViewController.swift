//
//  StartViewController.swift
//  Police
//
//  Created by Aluminum on 7/21/17.
//  Copyright © 2017 Crossover. All rights reserved.
//

import UIKit
import Localize_Swift

class StartViewController: UIViewController {
    private let TAG: String = "StartViewController"
    
    // MARK: - Properties
    
    @IBOutlet var loadIndicator: UIActivityIndicatorView!
    @IBOutlet var identifierLabel: UILabel!
    
    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        updateIdentifier()
        performStart()
    }
    
    func performStart() {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        self.loadIndicator.startAnimating()
        let startTask = DispatchQueue(label: "startTask")
        startTask.async {
            // Do we need this?
            Thread.sleep(forTimeInterval: 1)
            
            // Check database
            var dbHelper = PoliceDBHelper()
            dbHelper?.versionCheck()
            dbHelper = nil
            
            var segueIdentifier = "ShowSetup"
            if appDelegate.haveInstallationId() {
                // Update the language based on settings
                Localize.setCurrentLanguage(appDelegate.getLanguageText())
                segueIdentifier = "ShowMainMenu"
            }
            
            // Start the view controller at main thread
            DispatchQueue.main.async {
                self.loadIndicator.stopAnimating()
                self.performSegue(withIdentifier: segueIdentifier, sender: self)
            }
        }
    }
    
    func updateIdentifier() {
        self.identifierLabel.text = (UIApplication.shared.delegate as! AppDelegate).getInstallationId() ?? "\0"
    }

    // MARK: - Navigation
    
    @IBAction func unwindToStart(segue: UIStoryboardSegue) {
        // Restart as we have a identifier generated from setup
        updateIdentifier()
        performStart()
    }
}
