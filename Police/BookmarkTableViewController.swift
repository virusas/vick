//
//  BookmarkTableViewController.swift
//  Police
//
//  Created by Aluminum on 7/27/17.
//  Copyright © 2017 Crossover. All rights reserved.
//

import UIKit

class Bookmark {
    let mName: String
    let mChapter: Int
    let mChapterIndex: Int
    //let mImage: String
    
    init(_ bookName: String, _ chapter: Int, _ chapterIndex: Int) {
        mName = bookName
        mChapter = chapter
        //mImage = image
        mChapterIndex = chapterIndex
    }
}

class BookmarkTableViewCell: UITableViewCell {
    @IBOutlet var indexLabel: UILabel!
    @IBOutlet var chapterLabel: UILabel!
    //@IBOutlet var imagelabel: UIImageView?
}

class BookmarkTableViewController: BaseTableViewController, UIGestureRecognizerDelegate {
    
    // MARK: - Types
    
    private let TAG: String = "BookmarkTableViewController"
    
    // Constants for Storyboard/ViewControllers.
    static let storyboardName = "Main"
    static let viewControllerIdentifier = "BookmarkTableViewController"
    
    static let nibName: String = "BookmarkTableCell"
    static let tableViewCellIdentifier = "Bookmark Table Cell"

    // MARK: - Properties

    var emptyLabel: UILabel!
    var headerTitles = [String]()
    var dataSet = [[Bookmark]]()
    
    // MARK: - View Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.estimatedRowHeight = CGFloat(44)
        tableView.rowHeight = UITableViewAutomaticDimension
        
        // Required to use `dequeueReusableCellWithIdentifier(_:forIndexPath:)`.
        let nib = UINib(nibName: BookmarkTableViewController.nibName, bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: BookmarkTableViewController.tableViewCellIdentifier)
        
        // Allow long press action
        let longPressGesture:UILongPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(BookmarkTableViewController.handleLongPress(_:)))
        longPressGesture.minimumPressDuration = 0.5
        longPressGesture.delegate = self
        self.tableView.addGestureRecognizer(longPressGesture)
        
        emptyLabel = UILabel(frame: tableView.bounds)
        emptyLabel.text = "message_empty".localized()
        emptyLabel.textAlignment = .center
    }
    
    override func viewWillAppear(_ animated: Bool) {
        refreshList()
    }
    
    // MARK: - Database functions
    
    func getBookmarks() -> [[Bookmark]] {
        headerTitles.removeAll()
        var result = [[Bookmark]]()
        
        // Get our database
        var dbHelper = PoliceDBHelper()
        
        // Check book count
        var query = "SELECT \(PoliceContract.BookmarkEntry.COLUMN_NAME_BOOK) FROM \(PoliceContract.BookmarkEntry.TABLE_NAME) GROUP BY \(PoliceContract.BookmarkEntry.COLUMN_NAME_BOOK)"
        if let statement = dbHelper?.query(query) {
            while sqlite3_step(statement) == SQLITE_ROW {
                let name = String(cString: sqlite3_column_text(statement, 0))
                headerTitles.append(name)
            }
            sqlite3_finalize(statement)
        } else {
            NSLog("[\(TAG)] Unable to get book count")
        }
        
        // Make queries is not empty
        if !headerTitles.isEmpty {
            for bookName in headerTitles {
                query = "SELECT \(PoliceContract.BookmarkEntry.COLUMN_NAME_CHAPTER), \(PoliceContract.BookmarkEntry.COLUMN_NAME_CHAPTER_INDEX), \(PoliceContract.BookmarkEntry.COLUMN_NAME_IMAGE) FROM \(PoliceContract.BookmarkEntry.TABLE_NAME) WHERE \(PoliceContract.BookmarkEntry.COLUMN_NAME_BOOK) = '\(bookName)' ORDER BY \(PoliceContract.BookmarkEntry.COLUMN_NAME_CHAPTER_INDEX), \(PoliceContract.BookmarkEntry.COLUMN_NAME_CHAPTER)"
                if let statement = dbHelper?.query(query) {
                    // Store result into array
                    var bookmarks = [Bookmark]()
                    while sqlite3_step(statement) == SQLITE_ROW {
                        let bookName = bookName
                        let chapter = Int(sqlite3_column_int(statement, 0))
                        let chapterIndex = Int(sqlite3_column_int(statement, 1))
                        let image = String(sqlite3_column_int(statement, 2))
                        let bookmark = Bookmark(bookName, chapter, chapterIndex)
                        bookmarks.append(bookmark)
                    }
                    sqlite3_finalize(statement)
                    result.append(bookmarks)
                }
            }
        }
        
        dbHelper = nil
        return result
    }
    
    func deleteBookmark(_ bookmark: Bookmark) {
        var dbHelper = PoliceDBHelper()
        
        let statement = "DELETE FROM \(PoliceContract.BookmarkEntry.TABLE_NAME) WHERE \(PoliceContract.BookmarkEntry.COLUMN_NAME_BOOK) = '\(bookmark.mName)' AND \(PoliceContract.BookmarkEntry.COLUMN_NAME_CHAPTER) = \(bookmark.mChapter) AND \(PoliceContract.BookmarkEntry.COLUMN_NAME_CHAPTER_INDEX) = \(bookmark.mChapterIndex)"
        
        dbHelper?.execute(statement)
        dbHelper = nil
    }
    
    func refreshList() {
        dataSet = getBookmarks()
        tableView.backgroundView = dataSet.isEmpty ? emptyLabel : nil
        tableView.reloadData()
    }
    
    // MARK: - Gesture recongnizers
    
    @objc func handleLongPress(_ longPressGestureRecognizer: UILongPressGestureRecognizer) {
        if longPressGestureRecognizer.state == UIGestureRecognizerState.began {
            let touchPoint = longPressGestureRecognizer.location(in: self.view)
            if let indexPath = self.tableView.indexPathForRow(at: touchPoint) {
                let alert = UIAlertController(title: nil, message: "dialog_remove_bookmark".localized(), preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "action_cancel".localized(), style: .cancel, handler: nil))
                alert.addAction(UIAlertAction(title: "action_delete".localized(), style: .destructive, handler: { (action: UIAlertAction!) in
                    // Delete the row from the data source
                    self.deleteBookmark(self.dataSet[indexPath.section][indexPath.row])
                    self.dataSet[indexPath.section].remove(at: indexPath.row)
                    self.tableView.deleteRows(at: [indexPath], with: .fade)
                }))
                present(alert, animated: true, completion: nil)
            }
        }
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return dataSet.count
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSet[section].count
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let booksMgr = (UIApplication.shared.delegate as! AppDelegate).booksMgr
        return booksMgr?.getBook(headerTitles[section])?.mTitle
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: BookmarkTableViewController.tableViewCellIdentifier, for: indexPath) as! BookmarkTableViewCell
        
        // Configure the cell...
        let booksMgr = (UIApplication.shared.delegate as! AppDelegate).booksMgr
        let chapterIndex = dataSet[indexPath.section][indexPath.row]
        if let indexInfo = booksMgr?.getChapterIndex(chapterIndex.mName, chapterIndex.mChapter, chapterIndex.mChapterIndex) {
            cell.indexLabel.text = indexInfo.mTitle
            cell.chapterLabel.text = indexInfo.mOwningChapter.mTitle
            //cell.imagelabel?.image = UIImage(named:chapterIndex.mImage)
        } else {
            cell.indexLabel.text = "ERROR"
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let bookmark = dataSet[indexPath.section][indexPath.row]
        
        let booksMgr = (UIApplication.shared.delegate as! AppDelegate).booksMgr
        if let indexInfo = booksMgr?.getChapterIndex(bookmark.mName, bookmark.mChapter, bookmark.mChapterIndex) {
            let bookReaderViewController = BookReaderViewController.bookReaderViewControllerForChapterIndex(indexInfo)
            self.navigationController?.pushViewController(bookReaderViewController, animated: true)
        }
    }
}
