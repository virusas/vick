//
//  BooksManager.swift
//  Police
//
//  Created by Aluminum on 7/25/17.
//  Copyright © 2017 Crossover. All rights reserved.
//

import UIKit
import Foundation
import Localize_Swift

// Metadata for a book
class BookInfo {
    let mAssetName: String
    let mTitle: String
    //var mImage: String
    var mChapters = [ChapterInfo]()
    
    
    init(assetName: String, title: String) {
        self.mAssetName = assetName
        self.mTitle = title
        //self.mImage = image
    }
    
    func getChapter(_ chapterNum: Int) -> ChapterInfo? {
        return (0 <= chapterNum && chapterNum < mChapters.count) ? mChapters[chapterNum] : nil
    }
}

// Metadata for a chapter
class ChapterInfo: NSObject, NSCoding {
    enum CoderKeys: String {
        case owningBookKey
        case numKey
        case titleKey
        case imageKey
    }

    let mOwningBook: BookInfo
    
    let mNum: Int
    let mTitle: String
    let mImage: String
    var mIndexes = [ChapterIndexInfo]()
    
    init(owningBook: BookInfo, num: Int, title: String, image: String) {
        self.mOwningBook = owningBook
        self.mNum = num
        self.mTitle = title
        self.mImage = image
    }
    
    required init?(coder aDecoder: NSCoder) {
        mOwningBook = aDecoder.decodeObject(forKey: CoderKeys.owningBookKey.rawValue) as! BookInfo
        mNum = aDecoder.decodeInteger(forKey: CoderKeys.numKey.rawValue)
        mTitle = aDecoder.decodeObject(forKey: CoderKeys.titleKey.rawValue) as! String
        mImage = aDecoder.decodeObject(forKey: CoderKeys.imageKey.rawValue) as! String
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(mOwningBook, forKey: CoderKeys.owningBookKey.rawValue)
        aCoder.encode(mNum, forKey: CoderKeys.numKey.rawValue)
        aCoder.encode(mTitle, forKey: CoderKeys.titleKey.rawValue)
        aCoder.encode(mImage, forKey: CoderKeys.imageKey.rawValue)
    }
    
    func getChapterIndex(_ indexNum: Int) -> ChapterIndexInfo? {
        return (0 <= indexNum && indexNum < mIndexes.count) ? mIndexes[indexNum] : nil
    }
}



// Metadata for a chapter index
class ChapterIndexInfo: NSObject, NSCoding {
    enum CoderKeys: String {
        case owningChapterKey
        case numKey
        case titleKey
        case imageKey
        //case summaryKey
        case filePathKey
    }
    
    let mOwningChapter: ChapterInfo
    
    let mNum: Int
    let mTitle: String
    let mImage: String
    let mFilePath: String
    
    init(owningChapter: ChapterInfo, num: Int, title: String, image: String, filePath: String) {
        self.mOwningChapter = owningChapter
        self.mNum = num
        self.mTitle = title
        self.mFilePath = filePath
        self.mImage = image
    }
    
    required init?(coder aDecoder: NSCoder) {
        mOwningChapter = aDecoder.decodeObject(forKey: CoderKeys.owningChapterKey.rawValue) as! ChapterInfo
        mNum = aDecoder.decodeInteger(forKey: CoderKeys.numKey.rawValue)
        mTitle = aDecoder.decodeObject(forKey: CoderKeys.titleKey.rawValue) as! String
        mFilePath = aDecoder.decodeObject(forKey: CoderKeys.filePathKey.rawValue) as! String
        mImage = aDecoder.decodeObject(forKey: CoderKeys.imageKey.rawValue) as! String
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(mOwningChapter, forKey: CoderKeys.owningChapterKey.rawValue)
        aCoder.encode(mNum, forKey: CoderKeys.numKey.rawValue)
        aCoder.encode(mTitle, forKey: CoderKeys.titleKey.rawValue)
        aCoder.encode(mFilePath, forKey: CoderKeys.filePathKey.rawValue)
        aCoder.encode(mImage, forKey: CoderKeys.imageKey.rawValue)
    }
}

class BooksManager {
    private let TAG: String = "BooksManager"
    
    private var mCachedInfo = [BookInfo]()
    
    public init() {
        purgeCache()
    }
    
    public func purgeCache() {
        mCachedInfo.removeAll()
    }
    
    public func getBook(_ bookName: String) -> BookInfo? {
        // Check if we have it cached
        for book in mCachedInfo {
            if book.mAssetName == bookName {
                return book
            }
        }
        
        // Not cached, build the book
        if let book = buildBookInfo(bookName) {
            return book
        } else {
            NSLog("[\(TAG)] Unable to locale book \(bookName).")
            return nil
        }
    }
    
    public func getChapter(_ bookName: String, _ chapter: Int) -> ChapterInfo? {
        if let book = getBook(bookName) {
            return book.getChapter(chapter)
            //return book.getChapter(image)
        } else {
            NSLog("[\(TAG)] Unable to locale book \(bookName).")
            return nil
        }
    }
    
    public func getChapterIndex(_ bookName: String, _ chapter: Int, _ chapterIndex: Int) -> ChapterIndexInfo? {
        if let chapterInfo = getChapter(bookName, chapter) {
            return chapterInfo.getChapterIndex(chapterIndex)
            //return chapterInfo.getChapterIndex(image)
        } else {
            NSLog("[\(TAG)] Unable to locale chapter \(chapter) from book \(bookName).")
            return nil
        }
    }
    
    private func readJSONFile(_ filePath: String, _ fileType: String) -> [String: Any]? {
        var result: [String: Any]? = nil
        do {
            if let url = Bundle.main.url(forResource: filePath, withExtension: fileType) {
                let data = try Data(contentsOf: url)
                result = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            }
        } catch {
            NSLog("[\(TAG)] Error parsing file \(filePath).\(fileType).")
            print(error.localizedDescription)
        }
        return result
    }
    
    private func buildChapaterIndexInfo(_ bookName: String, _ owningChapter: ChapterInfo, _ chapterIndexName: String, _ num: Int, _ locale: String) -> ChapterIndexInfo? {
        let jsonChapterIndex = readJSONFile("ebooks/\(bookName)/\(chapterIndexName)", "json")
        NSLog("[\(TAG)] bookName = " + bookName)
        NSLog("[\(TAG)] chapterIndexName = " + chapterIndexName)
        NSLog("[\(TAG)] ebooks/\(bookName)/\(chapterIndexName)")
        if jsonChapterIndex == nil {
            NSLog("[\(TAG)] return nil")
            return nil
        }
        
        let title = (jsonChapterIndex!["title"] as! [String: Any])[locale] as! String
        
        let image = jsonChapterIndex!["image"] as! String
        return ChapterIndexInfo(owningChapter: owningChapter, num: num, title: title, image: image, filePath: chapterIndexName + "_" + locale)
    }
    
    private func buildChapterInfo(_ bookName: String, _ owningBook: BookInfo, _ chapterName: String, _ num: Int, _ locale: String) -> ChapterInfo? {
        let jsonChapter = readJSONFile("ebooks/\(bookName)/\(chapterName)", "json")
        NSLog("[\(TAG)] buildChapterInfo() -> ebooks/\(bookName)/\(chapterName)")
        if jsonChapter == nil {
            return nil
        }
        
        let title = (jsonChapter!["title"] as! [String: Any])[locale] as! String
        let image = jsonChapter!["image"] as! String
        let info: ChapterInfo = ChapterInfo(owningBook: owningBook, num: num, title: title, image: image)
        
        // Build indexes
        let jsonIndexes = jsonChapter!["indexes"] as! [String]
        if jsonIndexes.count <= 0 {
            NSLog("[\(TAG)] Chapter with no indexes inside!")
            return nil
        }
        
        // Build chapter indexes
        for i in 0 ..< jsonIndexes.count {
            let chapterIndexInfo: ChapterIndexInfo? = buildChapaterIndexInfo(bookName, info, jsonIndexes[i], i + 1, locale)
            if chapterIndexInfo == nil {
                NSLog("[\(TAG)] Error building chapter index info #\(i + 1)")
                return nil
            }
            info.mIndexes.insert(chapterIndexInfo!, at: i)
        }
        
        return info
    }
    
    private func buildBookInfo(_ bookName: String) -> BookInfo? {
        // Get locale settting
        var locale = Localize.currentLanguage()
        if locale == "zh-Hant" {
            // We need to warp this to zh-tw to cope with assets
            locale = "zh-tw"
        }
        
        
        NSLog("[\(TAG)] Building book info with path \"assets/ebooks/\(bookName)\", language \(locale)")
        let jsonBook = readJSONFile("ebooks/" + bookName + "/info", "json")
        //let jsonImage = readJSONFile("ebooks/" + bookName + "/info", "json")
        if jsonBook == nil {
            NSLog("[\(TAG)] bookinfo.json does not exist!")
            return nil
        }
        /*
        if jsonImage == nil {
            NSLog("[\(TAG)] image does not exist!")
            return nil
        }
       
        */
        // Create object and set title
        let result: BookInfo = BookInfo(assetName: bookName,
                                        title: (jsonBook!["title"] as! [String: Any])[locale] as! String)
        
        // Get chapters
        let jsonChapters = jsonBook!["chapters"] as! [String]
        if jsonChapters.count <= 0 {
            NSLog("[\(TAG)] Book with no chapters inside!")
            return nil
        }else{
            NSLog("[\(TAG)] Chapters count " + String(jsonChapters.count) )
        }
        for i in 0 ..< jsonChapters.count {
            NSLog("[\(TAG)] Chapters[" + String(i) + "] name = " + jsonChapters[i])
        }
        /*
        // Get images
        let jsonImages = jsonImage!["image"] as! [String]
        if jsonImages.count <= 0 {
            NSLog("[\(TAG)] Book with no images inside!")
            return nil
        }
         */
        // Build chapters
        for i in 0 ..< jsonChapters.count {
            let chapterInfo: ChapterInfo? = buildChapterInfo(bookName, result, jsonChapters[i], i + 1, locale)
            if chapterInfo == nil {
                NSLog("[\(TAG)] Error building chapter info #\(i + 1)")
                return nil
            }
            
            result.mChapters.insert(chapterInfo!, at: i)
        }
        
        // Complete info, cache it
        mCachedInfo.append(result)
        return result
    }
    
}
