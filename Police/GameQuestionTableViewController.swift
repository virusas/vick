//
//  GameQuestionTableViewController.swift
//  Police
//
//  Created by Aluminum on 8/18/17.
//  Copyright © 2017 Crossover. All rights reserved.
//

import UIKit

class GameQuestionTableViewCell: UITableViewCell {
    @IBOutlet var contentLabel: UILabel!
}

class GameQuestionTableViewController: UITableViewController {
    static let rowHeight = 32
    
    private let TAG: String = "GameQuestionTableViewController"
    
    var answers: [AnswerInfo]!
    var canSelect: [Bool]!
    
    class func gameQuestionTableForVC(_ answers: [AnswerInfo]) -> GameQuestionTableViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "GameQuestionTableViewController") as! GameQuestionTableViewController
        viewController.answers = answers
        viewController.canSelect = Array(repeating: true, count: answers.count)
        return viewController
    }
    
    override func viewDidLoad() {
        tableView.estimatedRowHeight = CGFloat(GameQuestionTableViewController.rowHeight)
        tableView.rowHeight = UITableViewAutomaticDimension
    }
    
    func calculateTableHeight() -> CGFloat {
        var result = CGFloat(0)
        for i in 0 ..< tableView.numberOfRows(inSection: 0) {
            result += tableView.rectForRow(at: IndexPath(row: i, section: 0)).height
        }
        return result
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return answers.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! GameQuestionTableViewCell

        // Configure the cell...
        cell.contentLabel?.text = answers[indexPath.row].mAnswer
        if !canSelect[indexPath.row] {
            cell.contentLabel?.textColor = UIColor.lightGray
        }
        cell.accessoryType = .none

        return cell
    }
    
    override func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        return canSelect[indexPath.row]
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // Remove all accessory on each section
        for i in 0 ..< tableView.numberOfRows(inSection: indexPath.section) {
            tableView.cellForRow(at: IndexPath(row: i, section: indexPath.section))?.accessoryType = .none
        }
        
        // Deselect selected
        tableView.deselectRow(at: indexPath, animated: false)
        tableView.cellForRow(at: indexPath)?.accessoryType = .checkmark
        
        let viewController = parent as? GameQuestionViewController
        if viewController == nil {
            NSLog("[\(TAG)] Unable to cast parent view controller to GameQuestionViewController")
            return
        }
        
        viewController!.selectedAnswer = indexPath.row
        viewController!.submitButton.isEnabled = viewController!.selectedAnswer != -1
    }
}
