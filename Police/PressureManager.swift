//
//  PressureManager.swift
//  Police
//
//  Created by Crossover on 11/6/2018.
//  Copyright © 2018年 Crossover. All rights reserved.
//

import Foundation
import Localize_Swift

class TestSectionInfo {
    let mName: String
    var mQuestions: [String]
    
    init(name: String) {
        mName = name
        mQuestions = [String]()
    }
}

class PressureManager {
    private static let TAG: String = "PressureManager"
    
    private var mSections = [TestSectionInfo]()
    
    init() {
        purgeCache()
    }
    
    func purgeCache() {
        // Get locale settting
        var locale = Localize.currentLanguage(  )
        if locale == "zh-Hant" {
            // We need to warp this to zh-tw to cope with assets
            locale = "zh-tw"
        }
        buildSectionInfo(locale)
    }
    
    private func buildSectionInfo(_ locale: String) {
        let jsonQuestions = JSONUtils.readJSONArray("pressure_test/questions", "json")
        if (jsonQuestions == nil || jsonQuestions!.count <= 0) {
            return
        }
        
        for testSection in jsonQuestions! {
            // Gather data
            let title = (testSection["title"] as! [String: Any])[locale] as! String
            let info = TestSectionInfo(name: title)
            
            // Gather questions
            let questions = (testSection["questions"] as! [String: Any])[locale] as! [String] 
            for question in questions {
                info.mQuestions.append(question)
            }
            
            // Finally, add to list
            mSections.append(info)
        }
    }
    
    func getTestSections() -> [TestSectionInfo] {
        return mSections
    }
}
