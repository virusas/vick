//
//  UserInfoTableViewCell.swift
//  Police
//
//  Created by Aluminum on 7/28/17.
//  Copyright © 2017 Crossover. All rights reserved.
//

import UIKit

class UserInfoTableViewCell: UITableViewCell {
    @IBOutlet weak var userImg: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userLv: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        let level = UserDefaults.standard.integer(forKey: "pref_user_level")
        var getImage: String
        let havImg = UserDefaults().string(forKey: "userImg")
        if  havImg != nil {
            getImage = havImg!
        }
        else {
            getImage = "pc_m"
            UserDefaults().set(getImage, forKey: "userImg")
        }
//        let getImage = UserDefaults().string(forKey: "userImg")!
        var image: String
        switch level {
        case 1:
            image = "\(getImage)_cert"
            userLv.text = "Level 1".localized()
        case 2:
            image = "\(getImage)_lanyard"
            userLv.text = "Level 2".localized()
        case 3:
            image = "\(getImage)_medal"
            userLv.text = "Level 3".localized()
        default:
            image = getImage
            userLv.text = "Level 0".localized()
        } 
        userImg.image = UIImage(named: image)
        userName.text = UserDefaults().string(forKey: "userName")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
