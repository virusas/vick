//
//  SetupViewController.swift
//  Police
//
//  Created by Aluminum Ken on 22/7/2017.
//  Copyright © 2017年 Crossover. All rights reserved.
//

import UIKit

class ValidateViewController: UIViewController, UITextViewDelegate {
    // MARK: - Properties
    
    @IBOutlet var passcodeField: UITextField!
    @IBOutlet var doneButton: UIBarButtonItem!

    // MARK: - View Life Cycle
    
    override func viewWillAppear(_ animated: Bool) {
        // Set navigation bar to translucent
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
    }
    
    // MARK: - UITextViewDelegate
    
    @objc func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.isEqual(passcodeField) {
            passcodeField.resignFirstResponder()
        }
        return true
    }
    
    // MARK: - Storyboard callbacks
    
    @IBAction func textFieldEditingChanged(_ sender: Any) {
        let senderField: UITextField = sender as! UITextField
        if senderField.isEqual(passcodeField) {
            doneButton.isEnabled = senderField.hasText
        }
    }
    
    @IBAction func nextPressed(_ sender: Any) {
        passcodeField.resignFirstResponder()
        
        let indicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
        indicator.hidesWhenStopped = true
        indicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
        let alert = UIAlertController(title: nil, message: NSLocalizedString("validate_progress", comment: "Dialog title"), preferredStyle: .alert)
        alert.view.addSubview(indicator)
        
        present(alert, animated: true, completion: nil)
        indicator.startAnimating()
        
        let validateTask = DispatchQueue(label: "validateTask")
        validateTask.async {
            DispatchQueue.main.async {
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                let validateResult: AppDelegate.ValidationResult = appDelegate.validatePassword(password: self.passcodeField.text!)
                
                
                alert.dismiss(animated: true, completion: {
                    switch validateResult {
                    case AppDelegate.ValidationResult.success :
                        appDelegate.setInstallationId()
                        DispatchQueue.main.async {
                            let storyboard = UIStoryboard(name: BookTableViewController.storyboardName, bundle: nil)
                            let viewController = storyboard.instantiateViewController(withIdentifier: "NameFieldViewController")
                            
                            // Replace root view controller
                            self.navigationController?.setViewControllers([viewController], animated: true)
                        }
                    case AppDelegate.ValidationResult.wrongPassword:
                        DispatchQueue.main.async {
                            let error = UIAlertController(title: nil, message: NSLocalizedString("validate_result_wrong_password", comment: "Dialog message"), preferredStyle: .alert)
                            error.addAction(UIAlertAction(title: "action_dismiss".localized(), style: .cancel, handler: nil))
                            self.present(error, animated: true, completion: nil)
                        }
                    case AppDelegate.ValidationResult.serverUnreachable:
                        DispatchQueue.main.async {
                            let error = UIAlertController(title: nil, message: NSLocalizedString("validate_result_server_unreachable", comment: "Dialog message"), preferredStyle: .alert)
                            error.addAction(UIAlertAction(title: "action_dismiss".localized(), style: .cancel, handler: nil))
                            self.present(error, animated: true, completion: nil)
                        }
                    }
                })
                }
        }
    }
}
