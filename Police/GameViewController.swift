//
//  GameViewController.swift
//  Police
//
//  Created by Aluminum on 8/15/17.
//  Copyright © 2017 Crossover. All rights reserved.
//

import UIKit

class GameViewController: UIViewController {
    private let TAG: String = "GameViewController"

//    @IBOutlet var progressionLabel: UILabel!
//    @IBOutlet var scoreLabel: UILabel!
    @IBOutlet var locationLabel: UILabel!
    @IBOutlet var backgroundImage: UIImageView!
    @IBOutlet var containerView: UIView!
    var parentVC: UIViewController!
    var stageInfo: StageInfo!
    
    var levelUp: Bool = false
    var curStep, score: Int
    var activeChildVC: UIViewController?
    
    var leaveButton: UIBarButtonItem?
    
    required init?(coder aDecoder: NSCoder) {
        curStep = -1
        score = 0
        super.init(coder: aDecoder)
    }
    
    class func gameViewControllerForStage(_ parentVC: UIViewController, _ stageInfo: StageInfo) -> UINavigationController? {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let navController = storyboard.instantiateViewController(withIdentifier: "GameNavController") as? UINavigationController
            else {
                return nil
        }

        if let viewController = navController.viewControllers.first as? GameViewController {
            viewController.parentVC = parentVC
            viewController.stageInfo = stageInfo
            return navController
        } else {
            return nil
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        if stageInfo.mSteps.count <= 0 {
            let alert = UIAlertController(title: nil, message: "error_missing_game_info".localized(), preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "action_dismiss".localized(), style: .cancel, handler: { (action: UIAlertAction!) in
                self.dismiss(animated: true, completion: nil)
            }))
            present(alert, animated: true, completion: nil)
            return
        }
        
        // Do any additional setup after loading the view.
//        scoreLabel.text = "\(score)"
        containerView.layer.masksToBounds = true
        containerView.layer.cornerRadius = 8

        nextStep()
    
        // Set up a new UIBarItem
    leaveButton = UIBarButtonItem(title: "action_leave".localized(), style: .plain, target: self, action: #selector(leaveGame(_:)))
       
        navigationItem.leftBarButtonItem = leaveButton
    
    }
    override func viewWillDisappear(_ animated: Bool) {
        if levelUp, let levelUpVC = GameLevelUpViewController.newInstance()  {
            parentVC.navigationController?.pushViewController(levelUpVC, animated: false)
        }
    }
    
    public func changeBackground(_ filePath: String) {
        DispatchQueue.global().async {
            if let image = UIImage(named: "games/\(self.stageInfo.mFilePath)/resources/\(filePath)", in: Bundle.main, compatibleWith: nil) {
                DispatchQueue.main.async {
                    self.backgroundImage.image = image
                }
            }
        }
    }
    
    public func nextStep() {
        curStep += 1
        
        if stageInfo.mSteps.count > curStep {
            changeStep(curStep)
        } else {
            saveGameState()
            displayResult()
        }
    }
    
    public func displayResult() {
        // Hide hud text
//        progressionLabel.isOpaque = false
//        scoreLabel.isOpaque = false
        locationLabel.isOpaque = false
        
        // Change container view
        if activeChildVC != nil {
            activeChildVC!.willMove(toParentViewController: nil)
            containerView.subviews[0].removeFromSuperview()
            activeChildVC!.removeFromParentViewController()
            activeChildVC = nil
        }
        
        activeChildVC = GameResultViewController.gameResultViewControllerForView(score, stageInfo.mLevelUp)
        activeChildVC!.willMove(toParentViewController: self)
        activeChildVC!.view.translatesAutoresizingMaskIntoConstraints = false
        containerView.addSubview(activeChildVC!.view)
        
        // Layout constraints
        NSLayoutConstraint(item: activeChildVC!.view, attribute: .top, relatedBy: .equal, toItem: containerView, attribute: .top, multiplier: 1.0, constant: 0.0).isActive = true
        NSLayoutConstraint(item: activeChildVC!.view, attribute: .leading, relatedBy: .equal, toItem: containerView, attribute: .leading, multiplier: 1.0, constant: 0.0).isActive = true
        NSLayoutConstraint(item: activeChildVC!.view, attribute: .trailing, relatedBy: .equal, toItem: containerView, attribute: .trailing, multiplier: 1.0, constant: 0.0).isActive = true
        NSLayoutConstraint(item: activeChildVC!.view, attribute: .bottom, relatedBy: .equal, toItem: containerView, attribute: .bottom, multiplier: 1.0, constant: 0.0).isActive = true
        
        addChildViewController(activeChildVC!)
        activeChildVC!.didMove(toParentViewController: self)
    }
    
    public func addScore(_ points: Int) {
        score += points
//        scoreLabel.text = "\(score)"
    }
    
    private func changeStep(_ index: Int) {
        if 0 > index || stageInfo.mSteps.count <= index {
            return
        }
        
        var viewController: UIViewController?
        let step = stageInfo.mSteps[index]
        switch step.mType {
        case 0:
            viewController = GameMessageViewController.gameMessageViewControllerForStep(step as! MessageInfo)
        case 1:
            viewController = GameQuestionViewController.gameQuestionViewControllerForStep(step as! QuestionInfo)
        default:
            NSLog("[\(TAG)] Unknown scene step type \(step.mType)")
        }
        
        // Change container view
        if viewController != nil {
            if activeChildVC != nil {
                activeChildVC!.willMove(toParentViewController: nil)
                containerView.subviews[0].removeFromSuperview()
                activeChildVC!.removeFromParentViewController()
                activeChildVC = nil
            }
            
            activeChildVC = viewController
            activeChildVC!.willMove(toParentViewController: self)
            activeChildVC!.view.translatesAutoresizingMaskIntoConstraints = false
            containerView.addSubview(activeChildVC!.view)
            
            // Layout constraints
            NSLayoutConstraint(item: activeChildVC!.view, attribute: .top, relatedBy: .equal, toItem: containerView, attribute: .top, multiplier: 1.0, constant: 0.0).isActive = true
            NSLayoutConstraint(item: activeChildVC!.view, attribute: .leading, relatedBy: .equal, toItem: containerView, attribute: .leading, multiplier: 1.0, constant: 0.0).isActive = true
            NSLayoutConstraint(item: activeChildVC!.view, attribute: .trailing, relatedBy: .equal, toItem: containerView, attribute: .trailing, multiplier: 1.0, constant: 0.0).isActive = true
            NSLayoutConstraint(item: activeChildVC!.view, attribute: .bottom, relatedBy: .equal, toItem: containerView, attribute: .bottom, multiplier: 1.0, constant: 0.0).isActive = true
            
            addChildViewController(activeChildVC!)
            activeChildVC!.didMove(toParentViewController: self)
        }
        
        // Change background image
        if step.mBackgroundImage != nil {
            changeBackground(step.mBackgroundImage!)
        }
        
        // Update text
        if step.mLocation != nil && step.mLocation != locationLabel.text {
            locationLabel.text = step.mLocation
        }
        
//        progressionLabel.text = "\(curStep + 1)/\(stageInfo.mSteps.count)"
    }
    
    private func saveGameState() {
        // Check if already completed
        if stageInfo.mCompleted {
            return
        }
        
        // Save record
        var dbHelper = PoliceDBHelper()
        let statement = "INSERT INTO \(PoliceContract.GameRecordEntry.TABLE_NAME) (\(PoliceContract.GameRecordEntry.COLUMN_NAME_CODENAME),\(PoliceContract.GameRecordEntry.COLUMN_NAME_VERSION),\(PoliceContract.GameRecordEntry.COLUMN_NAME_COMPLETED),\(PoliceContract.GameRecordEntry.COLUMN_NAME_COMPLETION_DATE)) VALUES ('\(stageInfo.mFilePath)','\(stageInfo.mVersion)',1,\(NSDate().timeIntervalSince1970 * 1000))"
        dbHelper?.execute(statement)
        dbHelper = nil
        levelUp = true
    }
    
    @objc func leaveGame(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
}
