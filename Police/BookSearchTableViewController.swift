//
//  BookSearchResultViewController.swift
//  Police
//
//  Created by Aluminum on 7/25/17.
//  Copyright © 2017 Crossover. All rights reserved.
//

import UIKit

class BookSearchTableViewController: BookBaseTableViewController {
    var filteredChapters = [ChapterInfo]()
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredChapters.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: BookBaseTableViewController.tableViewCellIdentifier, for: indexPath)
        
        // Configure the cell...
        let chapter = filteredChapters[indexPath.row]
        configureCell(cell as! BookTableViewCell, forChapter: chapter)
        return cell
    }
}
