//
//  WelcomeViewController.swift
//  Police
//
//  Created by Aluminum on 7/21/17.
//  Copyright © 2017 Crossover. All rights reserved.
//

import UIKit

class WelcomeViewController: UIViewController {
    
    // MARK: - Properties
    
    @IBOutlet var selectedButton: UIButton!
    
    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        /*
        // Make rounded
        selectedButton.layer.masksToBounds = true
        selectedButton.layer.borderWidth = 1
        selectedButton.layer.borderColor = view.tintColor.cgColor
        selectedButton.layer.cornerRadius = selectedButton.frame.height / 2
         */
    }
    
    // MARK: - Navigation
    @IBAction func selectedButtonClicked(_ sender: Any) {
        if let viewController = UIStoryboard(name: CharacterTableViewController.storyboardName, bundle: nil).instantiateViewController(withIdentifier: CharacterTableViewController.viewControllerIdentifier) as? CharacterTableViewController {
            viewController.currentTable = "pc"
            viewController.info = [
                cellData(text: "男警員", image: #imageLiteral(resourceName: "pc_m")),
                cellData(text: "女警員", image: #imageLiteral(resourceName: "pc_f"))
            ]
            if let navigator = navigationController {
                navigator.pushViewController(viewController, animated: true)
            }
        }
    }
    
    @IBAction func unwindWithSelection(segue: UIStoryboardSegue) {
        if let image = UserDefaults().string(forKey: "userImg"){
            selectedButton.setImage(UIImage(named: image), for: .normal)
        }
    }
}
