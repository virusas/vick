//
//  WelcomeNextViewController.swift
//  Police
//
//  Created by Crossover on 19/6/2018.
//  Copyright © 2018年 Crossover. All rights reserved.
//

import UIKit

class WelcomeViewNextController: UIViewController {
    // MARK: - View Life Cycle
    @IBOutlet weak var selectedButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Make rounded
        selectedButton.layer.masksToBounds = true
       /*
        selectedButton.layer.borderWidth = 1
        selectedButton.layer.borderColor = view.tintColor.cgColor
        selectedButton.layer.cornerRadius = selectedButton.frame.height / 2
        */
    }
    
    // MARK: - Navigation
    @IBAction func SelectedCharacter(_ sender: UIButton) {
        if let viewController = UIStoryboard(name: CharacterTableViewController.storyboardName, bundle: nil).instantiateViewController(withIdentifier: CharacterTableViewController.viewControllerIdentifier) as? CharacterTableViewController {
            viewController.currentTable = "psy"
            viewController.info = [
                cellData(text: "男心理學家", image: #imageLiteral(resourceName: "psy_m")),
                cellData(text: "女心理學家", image:#imageLiteral(resourceName: "psy_f"))
            ]
            if let navigator = navigationController {
                navigator.pushViewController(viewController, animated: true)
            }
        }
    }
    
    @IBAction func unwindWithSelection(segue: UIStoryboardSegue) {
        if let image = UserDefaults().string(forKey: "psyImg"){
            selectedButton.setImage(UIImage(named: image), for: .normal)
        }
    }
}
